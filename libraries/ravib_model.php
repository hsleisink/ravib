<?php
	abstract class ravib_model extends Banshee\model {
		private $aes = null;
		protected $organisation_id = null;

		/* Constructor
		 *
		 * INPUT: object database, object settings, object user, object page, object output[, object language]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);

			$cookie = new \Banshee\secure_cookie($this->settings);
			if (isset($_SESSION["advisor_organisation_id"])) {
				if ($this->valid_advisor_id($_SESSION["advisor_organisation_id"])) {
					$this->aes = new \Banshee\Protocol\AES256($cookie->advisor_crypto_key);
					$this->organisation_id = $_SESSION["advisor_organisation_id"];
				} else {
					$this->borrow("adviseur")->deactivate_role();
				}
			}

			if (($this->organisation_id == null) && ($this->user != null)) {
				$this->aes = new \Banshee\Protocol\AES256($cookie->crypto_key);
				$this->organisation_id = $this->user->organisation_id;
			}
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "organisation_id": return $this->organisation_id;
			}

			return null;
		}

		private function valid_advisor_id($organisation_id) {
			$query = "select count(*) as count from advisors where user_id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $this->user->id, $organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"] > 0;
		}

		public function get_case($case_id) {
			$query = "select * from cases where id=%d and organisation_id=%d limit 1";

			if (($result = $this->db->execute($query, $case_id, $this->organisation_id)) == false) {
				return false;
			}

			$this->decrypt($result[0], "name", "organisation", "scope", "impact", "interests");

			return $result[0];
		}

		public function get_standard($standard_id) {
			if (($standard = $this->db->entry("control_standards", $standard_id)) == false) {
				return false;
			}

			return $standard["name"];
		}

		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		public function encrypt(&$data) {
			if (is_false(ENCRYPT_DATA)) {
				return;
			}

			if (func_num_args() > 1) {
				$keys = func_get_args();
				array_shift($keys);
				$keys = array_flatten($keys);
			} else {
				$keys = array_keys($data);
			}

			foreach ($keys as $key) {
				if (isset($data[$key]) == false) {
					$data[$key] = "";
				} else if ($data[$key] != "") {
					$data[$key] = $this->aes->encrypt($data[$key]);
				}
			}
		}

		public function decrypt(&$data) {
			if (is_false(ENCRYPT_DATA)) {
				return;
			}

			if (func_num_args() > 1) {
				$keys = func_get_args();
				array_shift($keys);
				$keys = array_flatten($keys);
			} else {
				$keys = array_keys($data);
			}

			foreach ($keys as $key) {
				if (($data[$key] ?? null) != "") {
					$data[$key] = $this->aes->decrypt($data[$key]);
				}
			}
		}
	}
?>
