<?php
	define("RAVIB_VERSION", "3.0");
	define("DATABASE_VERSION", 5);

	define("UPDATE_TABLES", array("languages", "menu", "pages"));

	define("ORGANISATION_ADMIN_ROLE_ID", 2);

	/* Crypto settings
	 */
	define("RSA_KEY_SIZE", 4096);
	define("CRYPTO_KEY_SIZE", 32);

	define("ASSET_VALUE", array(
		# confidentiality
		array(
			#  availability
			array(0, 0, 1), #
			array(0, 0, 1), # integrity
			array(1, 1, 2)  #
		), array(
			array(0, 0, 1),
			array(0, 1, 2),
			array(1, 2, 2)
		), array(
			array(1, 1, 2),
			array(1, 2, 2),
			array(2, 2, 2)
		), array(
			array(1, 1, 2),
			array(1, 2, 2),
			array(2, 2, 2)
		)
	));

	define("ACTOR_THREAT", array(
		# resources
		array(
			#     chance
			array(1, 1, 2), #
			array(0, 1, 1), # knowledge
			array(0, 0, 1), #
		), array(
			array(0, 1, 1),
			array(0, 1, 2),
			array(0, 2, 2),
		), array(
			array(1, 2, 2),
			array(2, 2, 3),
			array(2, 3, 4)
		), array(
			array(1, 2, 2),
			array(2, 3, 3),
			array(2, 4, 4)
		)
	));

	define("RISK_MATRIX", array(
		#        impact
		array(0, 0, 0, 1, 2),  #
		array(0, 0, 1, 2, 2),  #
		array(0, 1, 2, 2, 3),  # chance
		array(0, 1, 2, 3, 3),  #
		array(1, 1, 2, 3, 3)   #
	));

	define("RISK_ACCEPT", 4);

	if (($_SESSION["language"] ?? null) == "nl") {
		define("ASSET_LOCATIONS", array(
			"intern" => "intern (op eigen systeem)",
			"extern" => "extern (in eigen beheer, maar op systeem van 3e partij)",
			"saas"   => "SAAS oplossing"));
		define("ASSET_VALUE_LABELS", array("normaal", "belangrijk", "essentieel"));

		define("ACTOR_CHANCE", array("gering", "mogelijk", "waarschijnlijk"));
		define("ACTOR_KNOWLEDGE", array("basis", "kundig", "expert"));
		define("ACTOR_RESOURCES", array("n.v.t.", "beperkt", "veel", "onbeperkt"));
		define("ACTOR_THREAT_LABELS", array("verwaarloosbaar", "gering", "gemiddeld", "bedreigend", "zeer bedreigend"));

		define("RISK_MATRIX_CHANCE", array("zeer klein", "klein", "gemiddeld", "groot", "zeer groot"));
		define("RISK_MATRIX_IMPACT", array("minimaal", "klein", "gemiddeld", "groot", "desastreus"));
		define("RISK_MATRIX_LABELS", array("laag", "midden", "hoog", "kritiek"));
		define("RISK_HANDLE_LABELS", array("behandelen", "ontwijken", "verweren", "accepteren"));

		define("AVAILABILITY_SCORE", array("normaal", "belangrijk", "cruciaal"));
		define("INTEGRITY_SCORE", array("normaal", "belangrijk", "cruciaal"));
		define("CONFIDENTIALITY_SCORE", array("publiek", "intern", "vertrouwelijk", "geheim"));

		define("CHANCE_FREQUENCY", array("> jaarlijks", "jaarlijks", "maandelijks", "wekelijks", "dagelijk"));
		define("CONTROL_REDUCES", array("kans", "impact", "kans & impact"));

		define("DEFAULT_IMPACT", array(
			"Financieel: < {E}1.000 / Imago: individueel",
			"Financieel: {E}1.000 - {E}10.000 / Imago: persoonlijke kring",
			"Financieel: {E}10.000 - {E}100.000 / Imago: plaatselijke pers",
			"Financieel: {E}100.000 - {E}1.000.000 / Imago: regionale pers",
			"Financieel: > {E}1.000.000 / Imago: landelijke pers"));

		define("DAYS_OF_WEEK", array("Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaturdag", "Zondag"));
		define("MONTHS_OF_YEAR", array("Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"));
	} else {
		define("ASSET_LOCATIONS", array(
			"internal" => "internal (at own server)",
			"external" => "external (maintained by you, but hosted at third party server)",
			"saas"     => "SAAS solution"));
		define("ASSET_VALUE_LABELS", array("normal", "important", "essential"));

		define("ACTOR_CHANCE", array("minor", "possible", "probable"));
		define("ACTOR_KNOWLEDGE", array("basic", "skilled", "expert"));
		define("ACTOR_RESOURCES", array("n/a", "limited", "many", "unlimited"));
		define("ACTOR_THREAT_LABELS", array("negligible", "minor", "average", "threatening", "very threatening"));

		define("RISK_MATRIX_CHANCE", array("very low", "low", "average", "high", "very high"));
		define("RISK_MATRIX_IMPACT", array("minimal", "small", "average", "big", "disastrous"));
		define("RISK_MATRIX_LABELS", array("low", "medium", "high", "critical"));
		define("RISK_HANDLE_LABELS", array("treat", "avoid", "resist", "accept"));

		define("AVAILABILITY_SCORE", array("normal", "important", "crucial"));
		define("INTEGRITY_SCORE", array("normal", "important", "crucial"));
		define("CONFIDENTIALITY_SCORE", array("public", "internal", "confidential", "secret"));

		define("CHANCE_FREQUENCY", array("> yearly", "yearly", "monthly", "weekly", "daily"));
		define("CONTROL_REDUCES", array("chance", "impact", "chance & impact"));

		define("DEFAULT_IMPACT", array(
			"Financial: < {E}1.000 / Image: individual",
			"Financial: {E}1.000 - {E}10.000 / Image: direct contacts",
			"Financial: {E}10.000 - {E}100.000 / Image: local news",
			"Financial: {E}100.000 - {E}1.000.000 / Image: regional news",
			"Financial: > {E}1.000.000 / Image: national news"));

		define("DAYS_OF_WEEK", array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"));
		define("MONTHS_OF_YEAR", array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"));
	}
?>
