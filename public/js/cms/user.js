function password_field() {
	if ($('input#generate:checked').length > 0) {
		$('input#password').val('');
		$('input#password').prop('disabled', true);
	} else {
		$('input#password').prop('disabled', false);
	}
}

function set_authenticator_code() {
	clear_authenticator_code();

	$.get('/cms/user/authenticator', function(data) {
		var username = encodeURIComponent($('input#username').val());
		var totp = encodeURIComponent($(data).find('secret').text());

		$('input#secret').val(totp);
		$('div.qrcode').append('<img src="/cms/user/totp/' + username + '/' + totp + '" />');
	});
}

function clear_authenticator_code() {
	$('input#secret').val('');
	$('div.secret_set').remove();
	$('div.qrcode').empty();
}
