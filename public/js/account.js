function set_authenticator_code() {
	clear_authenticator_code();

	$.get('/account/authenticator', function(data) {
		var totp = encodeURIComponent($(data).find('secret').text());

		$('input#secret').val(totp);
		$('div.qrcode').append('<img src="/account/totp/' + totp + '" />');
	});
}

function clear_authenticator_code() {
	$('input#secret').val('');
	$('div.secret_set').remove();
	$('div.qrcode').empty();
}
