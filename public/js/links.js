function show_threats() {
	$("#threat").addClass("selected");
	$("#control").removeClass("selected");

	$(".threats").show();
	$(".controls").hide();

	location.href = "#threats";
}

function show_controls() {
	$("#threat").removeClass("selected");
	$("#control").addClass("selected");

	$(".threats").hide();
	$(".controls").show();

	location.href = "#controls";
}

$(document).ready(function() {
	if (window.location.hash.length > 0) {
		if (window.location.hash == '#controls') {
			show_controls();
		}
	}
});
