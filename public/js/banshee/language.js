var language_global = {};
var language_module = {};

$(document).ready(function() {
	$('language global *').each(function() {
		var name = $(this).prop('tagName').toLowerCase();
		var text = $(this).text();

		language_global[name] = text;
	});

	$('language module *').each(function() {
		var name = $(this).prop('tagName').toLowerCase();
		var text = $(this).text();

		language_module[name] = text;
	});
});
