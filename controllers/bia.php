<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class bia_controller extends ravib_controller {
		private function show_overview() {
			if (($items = $this->model->get_items()) === false) {
				$this->view->add_tag("result", "Database error.", array("url" => "dashboard"));
				return;
			}

			$locations = array_keys(ASSET_LOCATIONS);

			$this->view->open_tag("overview");
			foreach ($items as $item) {
				$item["availability"] = AVAILABILITY_SCORE[$item["availability"] - 1] ?? "";
				$item["confidentiality"] = CONFIDENTIALITY_SCORE[$item["confidentiality"] - 1] ?? "";
				$item["integrity"] = INTEGRITY_SCORE[$item["integrity"] - 1] ?? "";
				$item["value"] = ASSET_VALUE_LABELS[$item["value"]];
				$item["owner"] = $this->language->global_text(show_boolean($item["owner"]));
				$item["personal_data"] = $this->language->global_text(show_boolean($item["personal_data"]));
				$item["location"] = $locations[$item["location"]];

				$this->view->record($item, "item");
			}
			$this->view->close_tag();
		}

		private function add_list($name, $list) {
			$this->view->open_tag($name);
			foreach ($list as $label) {
				$this->view->add_tag("label", $label);
			}
			$this->view->close_tag();
		}

		private function show_bia_form($item) {
			$this->view->open_tag("edit");

			$this->add_list("availability", AVAILABILITY_SCORE);
			$this->add_list("confidentiality", CONFIDENTIALITY_SCORE);
			$this->add_list("integrity", INTEGRITY_SCORE);
			$this->add_list("location", ASSET_LOCATIONS);

			$item["owner"] = show_boolean($item["owner"] ?? false);
			$item["personal_data"] = show_boolean($item["personal_data"] ?? false);

			$this->view->record($item, "item");

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_help_button();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_save_information_system")) {
					/* Save item
					 */
					if ($this->model->save_okay($_POST) == false) {
						$this->show_bia_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create item
						 */
						if ($this->model->create_item($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_creating_information_system"));
							$this->show_bia_form($_POST);
						} else {
							$this->user->log_action("bia created");
							$this->show_overview();
						}
					} else {
						/* Update item
						 */
						if ($this->model->update_item($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_updating_information_system"));
							$this->show_bia_form($_POST);
						} else {
							$this->user->log_action("bia updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_information_system")) {
					/* Delete item
					 */
					if ($this->model->delete_okay($_POST["id"]) == false) {
						$this->show_bia_form($_POST);
					} else if ($this->model->delete_item($_POST["id"]) == false) {
						$this->view->add_message($this->language->module_text("error_deleting_information_system"));
						$this->show_bia_form($_POST);
					} else {
						$this->user->log_action("bia deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New item
				 */
				$item = array();
				$this->show_bia_form($item);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit item
				 */
				if (($item = $this->model->get_item($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", $this->language->module_text("error_information_system_not_found"));
				} else {
					$this->show_bia_form($item);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
