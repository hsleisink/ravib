<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_controls_controller extends ravib_controller {
		protected $prevent_repost = false;

		private function show_overview() {
			if (($risks = $this->model->get_case_risks($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("risks", array("accept" => RISK_ACCEPT));
			foreach ($risks as $risk) {
				$risk["risk_value"] = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
				$risk["risk_label"] = RISK_MATRIX_LABELS[$risk["risk_value"]];
				$risk["handle_value"] = $risk["handle"];

				$risk_accepted_error = ($risk["handle"] == RISK_ACCEPT) && ($risk["controls"] > 0);
				$risk_not_accepted_error = ($risk["handle"] != RISK_ACCEPT) && ($risk["controls"] == 0);
				$risk["warning"] = show_boolean($risk_accepted_error || $risk_not_accepted_error);

				$risk["chance"] = RISK_MATRIX_CHANCE[$risk["chance"] - 1];
				$risk["impact"] = RISK_MATRIX_CHANCE[$risk["impact"] - 1];
				$risk["handle"] = RISK_HANDLE_LABELS[$risk["handle"] - 1];
				$this->view->record($risk, "risk");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_controls_form($risk_id, $selected) {
			if (($case_threat = $this->model->get_case_risk($risk_id, $this->case["id"])) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($standard = $this->model->get_standard($this->case["standard_id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($controls = $this->model->get_controls_standard($this->case["standard_id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($threats = $this->model->get_threats()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->add_javascript("case/controls.js");

			$this->view->open_tag("edit");

			$this->view->open_tag("controls", array("standard" => $standard));
			foreach ($controls as $control) {
				$effective = $this->model->effective_control($control["reduces"], $case_threat["handle"]);

				$control["effective"] = show_boolean($effective);
				$control["reduces"] = CONTROL_REDUCES[$control["reduces"]];
				$control["selected"] = show_boolean(in_array($control["id"], $selected));
				$this->view->record($control, "control");
			}
			$this->view->close_tag();

			$case_threat["risk_value"] = RISK_MATRIX[$case_threat["chance"] - 1][$case_threat["impact"] - 1];
			$case_threat["risk_label"] = RISK_MATRIX_LABELS[$case_threat["risk_value"]];
			$case_threat["chance"] = RISK_MATRIX_CHANCE[$case_threat["chance"] - 1];
			$case_threat["impact"] = RISK_MATRIX_IMPACT[$case_threat["impact"] - 1];
			$case_threat["handle"] = RISK_HANDLE_LABELS[$case_threat["handle"] - 1];
			$this->view->record($case_threat, "threat");

			$this->view->open_tag("threats");
			$this->view->add_tag("threat", "-- none --", array("id" => 0));
			foreach ($threats as $threat) {
				$text = $threat["number"].". ".$threat["threat"];
				$this->view->add_tag("threat", $text, array("id" => $threat["id"]));
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($this->page->ajax_request) {
				if (valid_input($this->page->parameters[0], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
					if (($controls = $this->model->get_controls_threat($this->page->parameters[0])) !== false) {
						foreach ($controls as $control) {
							$this->view->add_tag("control", $control["control_id"]);
						}
					}
				}
				return;
			}

			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->save_controls($_POST, $this->case) == false) {
					$this->show_controls_form($_POST["case_risk_id"], $_POST["selected"]);
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_numeric(1)) {
				/* Edit controls
				 */
				if (($selected = $this->model->get_selected_controls($this->page->parameters[1], $case_id)) === false) {
					$this->view->add_tag("result", "Risk not found.");
				} else {
					$this->show_controls_form($this->page->parameters[1], $selected);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
