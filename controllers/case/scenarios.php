<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_scenarios_controller extends ravib_controller {
		private function show_overview() {
			if (($scenarios = $this->model->get_scenarios($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("scenarios");
			foreach ($scenarios as $scenario) {
				$this->view->record($scenario, "scenario");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_scenario_form($scenario) {
			if (($actors = $this->model->get_actors()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($bia = $this->model->get_bia()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($risks = $this->model->get_risks($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("edit");

			$this->view->record($scenario, "scenario");

			$this->view->open_tag("actors");
			foreach ($actors as $actor) {
				$threat_level = $this->model->actor_threat($actor);
				$actor["chance"] = ACTOR_CHANCE[$actor["chance"] - 1];
				$actor["knowledge"] = ACTOR_KNOWLEDGE[$actor["knowledge"] - 1];
				$actor["resources"] = ACTOR_RESOURCES[$actor["resources"] - 1];
				$actor["threat_level"] = $threat_level;
				$actor["threat"] = ACTOR_THREAT_LABELS[$threat_level];

				$this->view->record($actor, "actor");
			}
			$this->view->close_tag();

			$this->view->open_tag("bia");
			foreach ($bia as $item) {
				$item["impact"] = unescaped_output($item["impact"]);
				$this->view->record($item, "item");
			}
			$this->view->close_tag();

			$this->view->open_tag("risks");
			foreach ($risks as $risk) {
				$risk["risk_value"] = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
				$risk["risk_label"] = RISK_MATRIX_LABELS[$risk["risk_value"]];

				$risk["chance"] = RISK_MATRIX_CHANCE[$risk["chance"] - 1];
				$risk["impact"] = RISK_MATRIX_IMPACT[$risk["impact"] - 1];
				$risk["handle"] = RISK_HANDLE_LABELS[$risk["handle"] - 1];

				$this->view->record($risk, "risk");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_save_scenario")) {
					/* Save scenario
					 */
					if ($this->model->save_okay($_POST) == false) {
						$this->show_scenario_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create scenario
						 */
						if ($this->model->create_scenario($_POST, $case_id) === false) {
							$this->view->add_message("Error while creating scenario.");
							$this->show_scenario_form($_POST);
						} else {
							$this->user->log_action("scenario %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update scenario
						 */
						if ($this->model->update_scenario($_POST, $case_id) === false) {
							$this->view->add_message("Error while updating scenario.");
							$this->show_scenario_form($_POST);
						} else {
							$this->user->log_action("scenario %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_scenario")) {
					/* Delete scenario
					 */
					if ($this->model->delete_scenario($_POST["id"], $case_id) === false) {
						$this->view->add_message("Error while deleting scenario.");
						$this->show_scenario_form($_POST);
					} else {
						$this->user->log_action("scenario %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(1, "new")) {
				/* New scenario
				 */
				$scenario = array();
				$this->show_scenario_form($scenario);
			} else if ($this->page->parameter_numeric(1)) {
				/* Edit scenario
				 */
				if (($scenario = $this->model->get_scenario($this->page->parameters[1], $case_id)) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_scenario_not_found"));
				} else {
					$this->show_scenario_form($scenario);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
