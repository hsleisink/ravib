<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_risks_controller extends ravib_controller {
		private function show_overview() {
			if (($risks = $this->model->get_case_risks($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("risks");
			foreach ($risks as $risk) {
				$risk["chance"] = RISK_MATRIX_CHANCE[$risk["chance"] - 1];
				$risk["impact"] = RISK_MATRIX_IMPACT[$risk["impact"] - 1];
				$risk["handle"] = RISK_HANDLE_LABELS[$risk["handle"] - 1];
				$this->view->record($risk, "risk");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_risk_template_form() {
			if (($templates = $this->model->get_threat_templates()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->model->get_categories()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$category_id = 0;

			$this->view->open_tag("templates");
			foreach ($templates as $template) {
				if ($template["category_id"] != $category_id) {
					$category_id = $template["category_id"];
					$template["category"] = $categories[$category_id]["name"];
				}
				$this->view->record($template, "template");
			}
			$this->view->close_tag();
		}

		private function show_risk_form($risk) {
			$cia = array("confidentiality", "integrity", "availability");

			if (isset($risk["template_id"])) {
				if (($template = $this->model->get_threat_template($risk["template_id"], $this->case["standard_id"])) != false) {
					foreach ($cia as $s) {
						if ($template[$s] == "") {
							$risk[$s] = "-";
						} else {
							$risk[$s] = $template[$s];
						}
					}

					$risk["template"] = $template["threat"];
					$risk["description"] = $template["description"];
					$risk["controls"] = $template["controls"];
					$risk["controls"]["standard"] = $template["standard"];
				}
			}

			if (($scope = $this->model->get_scope($this->case["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($actors = $this->model->get_actors()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("edit");

			/* Actors
			 */
			$this->view->open_tag("actors");
			$this->view->add_tag("actor", "", array("id" => 0));
			foreach ($actors as $actor) {
				$label = $actor["name"]." - Threat level: ".$actor["threat"];
				$this->view->add_tag("actor", $label, array("id" => $actor["id"]));
			}
			$this->view->close_tag();

			/* Chance values
			 */
			$this->view->open_tag("chance");
			$this->view->add_tag("option", "", array("value" => 0));
			foreach (RISK_MATRIX_CHANCE as $value => $label) {
				$this->view->add_tag("option", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			/* Impact values
			 */
			$this->view->open_tag("impact");
			$this->view->add_tag("option", "", array("value" => 0));
			foreach (RISK_MATRIX_IMPACT as $value => $label) {
				$this->view->add_tag("option", $label, array("value" => $value + 1));
			}
			$this->view->close_tag();

			/* Handle values
			 */
			$this->view->open_tag("handle");
			$this->view->add_tag("option", "", array("value" => "0"));
			foreach (RISK_HANDLE_LABELS as $i => $option) {
				$this->view->add_tag("option", $option, array("value" => $i + 1));
			}
			$this->view->close_tag();

			/* Scope
			 */
			if (is_array($risk["risk_scope"] ?? false) == false) {
				$risk["risk_scope"] = array();
			}

			$this->view->open_tag("scope");
			foreach ($scope as $system) {
				$system["availability"] = CONFIDENTIALITY_SCORE[$system["availability"] - 1] ?? "";
				$system["integrity"] = INTEGRITY_SCORE[$system["integrity"] - 1] ?? "";
				$system["confidentiality"] = CONFIDENTIALITY_SCORE[$system["confidentiality"] - 1] ?? "";
				$system["selected"] = show_boolean(in_array($system["id"], $risk["risk_scope"]));

				$this->view->record($system, "system");
			}
			$this->view->close_tag();

			$this->view->record($risk, "risk", array(), true);

			$this->view->close_tag();
		}

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_save_risk")) {
					/* Save risk
					 */
					if ($this->model->save_okay($_POST) == false) {
						$this->show_risk_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create risk
						 */
						if ($this->model->create_risk($_POST, $this->case) === false) {
							$this->view->add_message("Error creating threat.");
							$this->show_risk_form($_POST);
						} else {
							$this->user->log_action("threat %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update risk
						 */
						if ($this->model->update_risk($_POST, $case_id) === false) {
							$this->view->add_message("Error updating threat.");
							$this->show_risk_form($_POST);
						} else {
							$this->user->log_action("threat %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_risk")) {
					/* Delete risk
					 */
					if ($this->model->delete_risk($_POST["id"], $case_id) === false) {
						$this->view->add_message("Error deleting threat.");
						$this->show_risk_form($_POST);
					} else {
						$this->user->log_action("threat %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(1, "template")) {
				$this->show_risk_template_form();
			} else if ($this->page->parameter_value(1, "new")) {
				/* New risk
				 */
				$risk = array();
				if ($this->page->parameter_numeric(2)) {
					$risk["template_id"] = $this->page->parameters[2];
				}
				$this->show_risk_form($risk);
			} else if ($this->page->parameter_numeric(1)) {
				/* Edit risk
				 */
				if (($risk = $this->model->get_case_risk($this->page->parameters[1], $case_id)) == false) {
					$this->view->add_tag("result", "Risk not found.");
				} else {
					$this->show_risk_form($risk);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
