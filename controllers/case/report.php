<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_report_controller extends ravib_controller {
		protected $prevent_repost = false;

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (($report = $this->model->generate_report($this->case)) === false) {
					$this->view->add_tag("result", $this->language->module_text("error_generating_report"));
				} else {
					$this->view->disable();
					$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
					$report->Output($case_name." - ".$this->language->module_text("pdf_filename").".pdf", "I");
				}
			} else {
				$this->view->add_tag("overview");
			}
		}
	}
?>
