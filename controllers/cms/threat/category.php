<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_threat_category_controller extends Banshee\tablemanager_controller {
		protected $name = "Threat category";
		protected $back = "cms/threat";
		protected $icon = "threat_categories.png";
		protected $browsing = null;
	}
?>
