<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_control_export_controller extends Banshee\controller {
		public function execute() {
			if (($categories = $this->model->get_categories($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($controls = $this->model->get_controls($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$category_id = 0;

			$csv = new \banshee\CSVfile();

			foreach ($controls as $control) {
				list($cid) = explode(".", $control["number"]);
				if ($cid != $category_id) {
					$category_id = $cid;
					$csv->add_line($categories[$category_id]["name"]);
				}

				$csv->add_line($control["number"], $control["name"], CONTROL_REDUCES[$control["reduces"]]);
			}

			$csv->to_output($this->view);
		}
	}
?>
