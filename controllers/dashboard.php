<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class dashboard_controller extends ravib_controller {
		protected $prevent_repost = false;

		public function execute() {
			if (isset($_SESSION["advisor_organisation_id"]) != false) {
				if (($organisation = $this->model->get_organisation($_SESSION["advisor_organisation_id"])) != false) {
					$this->view->add_system_warning($this->language->module_text("act_as_advisor"), $organisation);
				}
			}

			if (($risks = $this->model->get_risks()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($controls = $this->model->get_controls()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($systems = $this->model->get_bia()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($progress = $this->model->get_progress()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("dashboard");

			$menu = array(
				"bia"     => $this->language->module_text("menu_bia"),
				"actors"  => $this->language->module_text("menu_actors"),
				"case"    => $this->language->module_text("menu_cases"),
				"data"    => $this->language->module_text("menu_data_management"),
				"advisor" => $this->language->module_text("menu_advisor"));

			$this->view->open_tag("menu");
			foreach ($menu as $link => $label) {
				if ($this->user->access_allowed($link)) {
					$this->view->add_tag("item", $label, array("link" => $link));
				}
			}
			$this->view->close_tag();

			$this->view->open_tag("risks");
			foreach ($risks as $i => $risk) {
				$this->view->add_tag("risk", RISK_MATRIX_LABELS[$i], array("value" => $risk));
			}
			$this->view->close_tag();

			$this->view->open_tag("controls");
			foreach ($controls as $i => $control) {
				$this->view->add_tag("control", RISK_MATRIX_LABELS[$i], array("value" => $control));
			}
			$this->view->close_tag();

			$this->view->open_tag("labels");
			foreach (array_reverse(RISK_MATRIX_LABELS) as $label) {
				$this->view->add_tag("label", ucfirst($label));
			}
			$this->view->close_tag();

			$this->view->open_tag("systems");
			foreach ($systems as $system) {
				$this->view->record($system, "system");
			}
			$this->view->close_tag();

			$this->view->add_tag("done", $progress["done"]);
			$this->view->add_tag("overdue", $progress["overdue"]);
			$this->view->add_tag("pending", $progress["pending"]);
			$this->view->add_tag("idle", $progress["idle"]);

			$this->view->close_tag();
		}
	}
?>
