<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class riskmatrix_controller extends Banshee\controller {
		public function execute() {
			$this->view->title = "Risk matrix";
			$this->view->description = "De risk matrix as being used within RAVIB.";
			$this->view->keywords = "risk matrix";

			$this->view->open_tag("matrix");

			$this->view->open_tag("row");
			$this->view->add_tag("cell", "");
			foreach (RISK_MATRIX_IMPACT as $impact) {
				$this->view->add_tag("cell", $impact);
			}
			$this->view->close_tag();

			$chance = 4;
			foreach (array_reverse(RISK_MATRIX) as $row) {
				$this->view->open_tag("row");
				$this->view->add_tag("cell", RISK_MATRIX_CHANCE[$chance--]);
				foreach ($row as $cell) {
					$this->view->add_tag("cell", RISK_MATRIX_LABELS[$cell], array("class" => "risk_".$cell));
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();
		}
	}
?>
