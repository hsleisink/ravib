<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class register_controller extends Banshee\splitform_controller {
		protected $button_submit = "Register";
		protected $back_page = "/";

		protected function prepare_account($data) {
			if (empty($data["username"])) {
				$this->model->set_value("username", strtolower($data["email"]));
			}
		}

		protected function prepare_code($data) {
			if (($_SESSION["register_email"] ?? null) == $data["email"]) {
				return;
			}

			$_SESSION["register_code"] = random_string(20, true);

			$email = new ravib_email($this->language->module_text("email_subject"));
			$email->set_message_fields(array(
				"CODE"    => $_SESSION["register_code"],
				"WEBSITE" => $this->settings->head_title));
			$email->message(file_get_contents("../extra/register_".$this->view->language.".txt"));
			$email->send($data["email"]);

			$_SESSION["register_email"] = $data["email"];
			$this->model->set_value("code", "");
		}

		public function execute() {
			if ($this->user->logged_in) {
				$this->view->add_tag("result", $this->language->module_text("already_have_account"), array("url" => ""));
				return;
			}

			$this->button_submit = $this->language->module_text("btn_submit");

			if ($_SERVER["REQUEST_METHOD"] == "GET") {
				$this->model->reset_form_progress();
			} else if (($_POST["splitform_current"] ?? null) == 2) {
				$_POST["username"] = strtolower($_POST["username"]);
			}

			$this->view->add_tag("ask_organisation", show_boolean(DEFAULT_ORGANISATION_ID == 0));

			parent::execute();
		}
	}
?>
