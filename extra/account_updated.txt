<p>Hello [FULLNAME],</p>
<p><a href="mailto:[ADMIN_EMAIL]">[ADMIN_NAME]</a> updated your account at the <a href="[PROTOCOL]://[HOSTNAME]/">RAVIB website</a>. You can use the following credentials to login:</p>
<p>Username: [USERNAME]<br>Password: [PASSWORD]</p>
