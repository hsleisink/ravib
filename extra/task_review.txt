<p>Hello [NAME],</p>
<p>The following task for concerning [CASE] has been assigned to [EXECUTOR].<p>
<p><b>[CONTROL]</b></p>
<p class="info">[INFORMATION]</p>
<p>You have been appointed as reviewer. You can mark this task as done via <a href="[LINK]">deze link</a>.</p>
