<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class dashboard_model extends ravib_model {
		private function all_done($progress) {
			if (count($progress) == 0) {
				return false;
			}

			foreach ($progress as $action) {
				if (isset($action["done"]) == false) {
					continue;
				}

				if (is_false($action["done"])) {
					return false;
				}
			}

			return true;
		}

		public function get_risks() {
			$query = "select r.id, r.chance, r.impact, c.id as case_id ".
			         "from case_risks r, cases c ".
			         "where r.case_id=c.id and c.organisation_id=%d and c.archived=%d ".
			         "and r.handle!=%d and r.handle!=%d";

			if (($risks = $this->db->execute($query, $this->organisation_id, NO, 0, RISK_ACCEPT)) === false) {
				return false;
			}

			$query = "select p.done from case_risk_control l ".
			         "left join case_progress p on p.case_id=%d and p.control_id=l.control_id ".
			         "where l.case_risk_id=%d";

			$result = array(0, 0, 0, 0);
			foreach ($risks as $risk) {
				if (($progress = $this->db->execute($query, $risk["case_id"], $risk["id"])) === false) {
					return false;
				}

				if ($this->all_done($progress)) {
					continue;
				}

				$risk_value = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
				$result[$risk_value]++;
			}

			return array_reverse($result, true);
		}

		public function get_controls() {
			$query = "select m.id, r.chance, r.impact, c.id as case_id ".
			         "from case_risk_control l, case_risks r, cases c, controls m ".
			         "where m.id=l.control_id and l.case_risk_id=r.id and r.case_id=c.id and c.standard_id=m.standard_id ".
			         "and c.organisation_id=%d and c.archived=%d and r.handle!=%d and r.handle!=%d";

			if (($controls = $this->db->execute($query, $this->organisation_id, NO, 0, RISK_ACCEPT)) === false) {
				return false;
			}

			$query = "select done from case_progress c where case_id=%d and control_id=%d";

			$urgency = array();
			foreach ($controls as $control) {
				if (($progress = $this->db->execute($query, $control["case_id"], $control["id"])) === false) {
					return false;
				}

				if ($this->all_done($progress)) {
					continue;
				}

				$urgency_value = RISK_MATRIX[$control["chance"] - 1][$control["impact"] - 1];
				if (($urgency[$control["id"]] ?? null) == null) {
					$urgency[$control["id"]] = $urgency_value;
				} else if ($urgency_value > $urgency[$control["id"]]) {
					$urgency[$control["id"]] = $urgency_value;
				}
			}

			$urgency = array_count_values($urgency);

			$result = array(0, 0, 0, 0);
			for ($i = 0; $i < 4; $i++) {
				$result[$i] += $urgency[$i] ?? 0;
			}

			return array_reverse($result, true);
		}

		private function sort_bia($bia1, $bia2) {
			return strcmp($bia1["item"], $bia2["item"]);
		}

		public function get_bia() {
			$query = "select id, item from bia where organisation_id=%d";

			if (($items = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach (array_keys($items) as $key) {
					$this->decrypt($items[$key], "item");
				}
			}

			uasort($items, array($this, "sort_bia"));

			$query_r = "select r.chance, r.impact, c.id as case_id, r.id ".
			           "from case_risks r, case_risk_bia b, cases c ".
			           "where r.id=b.case_risk_id and r.case_id=c.id and b.bia_id=%d and c.archived=%d";

			$query_p = "select p.done from case_risk_control l ".
			           "left join case_progress p on p.case_id=%d and p.control_id=l.control_id ".
			           "where l.case_risk_id=%d";

			foreach ($items as $i => $item) {
				$items[$i]["risk0"] = 0;
				$items[$i]["risk1"] = 0;
				$items[$i]["risk2"] = 0;
				$items[$i]["risk3"] = 0;

				if (($risks = $this->db->execute($query_r, $item["id"], NO)) === false) {
					return false;
				}

				foreach ($risks as $risk) {
					if (($progress = $this->db->execute($query_p, $risk["case_id"], $risk["id"])) === false) {
						return false;
					}

					if ($this->all_done($progress)) {
						continue;
					}

					$risk_value = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
					$items[$i]["risk".$risk_value]++;
				}
			}

			return $items;
		}

		public function get_progress() {
			$query = "select distinct s.control_id from case_risk_control s, case_risks r, controls m, cases c ".
			         "where s.case_risk_id=r.id and r.case_id=c.id and c.organisation_id=%d and c.archived=%d ".
			         "and s.control_id=m.id and m.standard_id=c.standard_id and r.handle!=%d and r.handle!=%d";
			if (($result = $this->db->execute($query, $this->organisation_id, NO, 0, RISK_ACCEPT)) === false) {
				return false;
			}
			$total = count($result);

			$controls = array();
			foreach ($result as $item) {
				array_push($controls, $item["control_id"]);
			}

			$result = array(
				"done"    => 0,
				"overdue" => 0,
				"pending" => 0,
				"idle"    => 0);

			if ($total == 0) {
				return $result;
			}

			$query = "select p.*, UNIX_TIMESTAMP(p.deadline) as deadline from case_progress p, cases c ".
					 "where p.case_id=c.id and c.organisation_id=%d";
			if (($progress = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			$today = strtotime("today");

			foreach ($progress as $item) {
				if (in_array($item["control_id"], $controls) == false) {
					continue;
				}
				if (is_true($item["done"])) {
					$result["done"]++;
				} else if ($item["deadline"] == null) {
					$result["idle"]++;
				} else if ($item["deadline"] < $today) {
					$result["overdue"]++;
				} else {
					$result["pending"]++;
				}
			}

			$result["idle"] = $total - $result["done"] - $result["overdue"] - $result["pending"];

			foreach (array_keys($result) as $key) {
				$result[$key] = round(100 * $result[$key] / $total, 1);
			}

			return $result;
		}
	}
?>
