<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class links_model extends Banshee\model {
		public function get_control_standards() {
			$query = "select * from control_standards where active=%d order by id desc";

			return $this->db->execute($query, YES);
		}

		public function get_threat_categories() {
			$query = "select id, %S as name from threat_categories";
			if (($categories = $this->db->execute($query, "name_".$this->view->language)) == false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[$category["id"]] = $category["name"];
			}

			return $result;
		}

		public function get_threats() {
			$query = "select id, number, %S as threat, %S as description, category_id, confidentiality, integrity, availability ".
			         "from threats order by number";
			if (($threats = $this->db->execute($query, "threat_".$this->view->language, "description_".$this->view->language)) === false) {
				return false;
			}

			$result = array();
			foreach ($threats as $threat) {
				foreach (array("availability", "integrity", "confidentiality") as $key) {
					if ($threat[$key] == "") {
						$threat[$key] = "-";
					}
				}
				$result[$threat["id"]] = $threat;
			}

			return $result;
		}

		public function get_controls($standard_id) {
			$query = "select id, standard_id, number, %S as name, reduces ".
			         "from controls where standard_id=%d";
			if (($controls = $this->db->execute($query, "name_".$this->view->language, $standard_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($controls as $control) {
				$result[$control["id"]] = $control;
			}

			return $result;
		}

		public function get_control_categories($standard_id) {
			$query = "select id, standard_id, number, %S as name ".
			         "from control_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, "name_".$this->view->language, $standard_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category;
			}

			return $result;
		}

		public function get_mitigation($standard_id) {
			$query = "select m.* from mitigation m, controls c ".
			         "where m.control_id=c.id and c.standard_id=%d";

			return $this->db->execute($query, $standard_id);
		}
	}
?>
