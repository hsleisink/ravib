<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class account_model extends Banshee\model {
		private $hashed = null;

		public function get_organisation() {
			if (($result = $this->db->entry("organisations", $this->user->organisation_id)) == false) {
				return false;
			}

			return $result["name"];
		}

		public function last_account_logs() {
			if (($fp = fopen("../logfiles/actions.log", "r")) == false) {
				return false;
			}

			$result = array();

			while (($line = fgets($fp)) !== false) {
				$parts = explode("|", chop($line));
				if (count($parts) < 4) {
					continue;
				}

				list($ip, $timestamp, $path, $user_id, $message) = $parts;

				if ($user_id == "-") {
					continue;
				} else if ($user_id != $this->user->id) {
					continue;
				}

				array_push($result, array(
					"ip"        => $ip,
					"timestamp" => $timestamp,
					"path"      => $path,
					"message"   => $message));
				if (count($result) > 15) {
					array_shift($result);
				}
			}

			fclose($fp);

			return array_reverse($result);
		}

		public function account_okay($account) {
			$result = true;

			if (trim($account["fullname"]) == "") {
				$this->view->add_message($this->language->module_text("error_fill_in_your_name"));
				$result = false;
			}

			if (valid_email($account["email"]) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_email_address"));
				$result = false;
			} else if (($check = $this->db->entry("users", $account["email"], "email")) != false) {
				if ($check["id"] != $this->user->id) {
					$this->view->add_message($this->language->module_text("error_email_address_exists"));
					$result = false;
				}
			}

			if (strlen($account["current"]) > PASSWORD_MAX_LENGTH) {
				$this->view->add_message($this->language->module_text("error_current_password_too_long"));
				$result = false;
			} else if (password_verify($account["current"], $this->user->password) == false) {
				$this->view->add_message($this->language->module_text("error_current_password_incorrect"));
				$result = false;
			}

			if ($account["password"] != "") {
				if (is_secure_password($account["password"], $this->view, $this->language) == false) {
					$result = false;
				} else if ($account["password"] != $account["repeat"]) {
					$this->view->add_message($this->language->module_text("error_password_mismatch"));
					$result = false;
				} else if (password_verify($account["password"], $this->user->password)) {
					$this->view->add_message($this->language->module_text("error_password_not_different"));
					$result = false;
				}

			}

			if (is_true(USE_AUTHENTICATOR)) {
				if ((strlen($account["authenticator_secret"]) > 0) && ($account["authenticator_secret"] != str_repeat("*", 16))) {
					if (valid_input($account["authenticator_secret"], Banshee\authenticator::BASE32_CHARS, 16) == false) {
						$this->view->add_message("Invalid authenticator code.");
						$result = false;
					}
				}
			}

			return $result;
		}

		public function update_account($account) {
			$keys = array("fullname", "email");

			if ($account["password"] != "") {
				array_push($keys, "password");
				array_push($keys, "status");
				if (ENCRYPT_DATA) {
					array_push($keys, "crypto_key");

					$aes = new \Banshee\Protocol\AES256($account["current"]);
					$crypto_key = $aes->decrypt($this->user->crypto_key);

					$aes = new \Banshee\Protocol\AES256($account["password"]);
					$account["crypto_key"] = $aes->encrypt($crypto_key);
				}

				$account["password"] = password_hash($account["password"], PASSWORD_ALGORITHM);
				$account["status"] = USER_STATUS_ACTIVE;
			}

			if (is_true(USE_AUTHENTICATOR)) {
				if ($account["authenticator_secret"] != str_repeat("*", 16)) {
					array_push($keys, "authenticator_secret");
					if (trim($account["authenticator_secret"]) == "") {
						$account["authenticator_secret"] = null;
					}
				}
			}

			return $this->db->update("users", $this->user->id, $account, $keys) !== false;
		}

		public function delete_okay() {
			if ($this->user->has_role(ORGANISATION_ADMIN_ROLE_ID)) {
				$query = "select count(*) as count from users u, user_role p ".
				         "where u.organisation_id=%d and u.id!=%d and u.id=p.user_id and p.role_id=%d";
				if (($result = $this->db->execute($query, $this->user->organisation_id, $this->user->id, ORGANISATION_ADMIN_ROLE_ID)) === false) {
					$this->view->add_message("Database error.");
					return false;
				}
				$admin_accounts = $result[0]["count"];

				$query = "select count(*) as count from users where organisation_id=%d and id!=%d";
				if (($result = $this->db->execute($query, $this->user->organisation_id, $this->user->id)) === false) {
					$this->view->add_message("Database error.");
					return false;
				}
				$user_accounts = $result[0]["count"] - $admin_accounts;

				if (($admin_accounts == 0) && ($user_accounts > 0)) {
					$this->view->add_message($this->language->module_text("error_last_admin"));
					return false;
				}
			}

			return true;
		}

		public function delete_account() {
			if ($this->user->is_admin) {
				return false;
			}

			if ($this->borrow("cms/user")->delete_user($this->user->id) === false) {
				return false;
			}

			/* Count users left
			 */
			$query = "select count(*) as count from users where organisation_id=%d";
			if (($result = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			/* No users left?
			 */
			if ($result[0]["count"] == 0) {
				/* Delete organisation
				 */
				if ($this->borrow("cms/organisation")->delete_organisation($this->user->organisation_id) === false) {
					return false;
				}
			}

			return true;
		}
	}
?>
