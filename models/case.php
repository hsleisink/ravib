<?php
	class case_model extends ravib_model {
		public function get_cases($archive) {
			$query = "select c.id, c.name, c.organisation, c.standard_id, UNIX_TIMESTAMP(c.date) as date, c.interests, s.name as standard, ".
			         "(select count(*) from case_scope where case_id=c.id) as scope_count, ".
			         "(select count(*) from case_risks where case_id=c.id) as risk_count, ".
					 "(select count(*) from case_risk_control m, case_risks r where m.case_risk_id=r.id and r.case_id=c.id) as control_count, ".
			         "(select count(*) from case_scenarios where case_id=c.id) as scenario_count, ".
			         "(select count(*) from case_progress where case_id=c.id) as progress_count ".
			         "from cases c, control_standards s ".
			         "where c.standard_id=s.id and c.organisation_id=%d and archived=%d order by date desc";

			if (($cases = $this->db->execute($query, $this->organisation_id, $archive ? YES : NO)) === false) {
				return false;
			}

			foreach ($cases as $c => $case) {
				$query = "select *, (select count(*) from case_risk_control m where m.case_risk_id=r.id) as control_count ".
				         "from case_risks r where r.case_id=%d";
				if (($risks = $this->db->execute($query, $case["id"])) === false) {
					return false;
				}

				$cases[$c]["controls_ok"] = true;
				foreach ($risks as $risk) {
					if ((($risk["control_count"] != 0) && ($risk["handle"] == RISK_ACCEPT)) ||
						(($risk["control_count"] == 0) && ($risk["handle"] != RISK_ACCEPT))) {
						$cases[$c]["controls_ok"] = false;
						break;
					}
				}
			}

			foreach (array_keys($cases) as $key) {
				$this->decrypt($cases[$key], "name", "organisation", "scope", "impact");
			}

			return $cases;
		}

		public function count_archive() {
			$query = "select count(*) as count from cases where organisation_id=%d and archived=%d";

			if (($result = $this->db->execute($query, $this->user->organisation_id, YES)) === false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_control_standards() {
			$query = "select * from control_standards";
			if ($this->user->is_admin == false) {
				$query .= " where active=%d";
			}
			$query .= " order by id desc";

			return $this->db->execute($query, YES);
		}

		public function start_crumb($case) {
			if ($case["scope_count"] == 0) {
				return "case/scope";
			}

			if ($case["interests"] == "") {
				return "case/interests";
			}

			if ($case["risk_count"] < 5) {
				return "case/risks";
			}

			if ($case["controls_ok"] == false) {
				return "case/controls";
			}

			if ($case["scenario_count"] == 0) {
				return "case/scenarios";
			}

			if ($case["progress_count"] > 0) {
				return "case/progress";
			}

			return "case/report";
		}

		public function save_okay($case) {
			$result = true;

			if (trim($case["name"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_name"));
				$result = false;
			}

			if (trim($case["organisation"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_organisation"));
				$result = false;
			}

			if ($result) {
				$query = "select count(*) as count from cases where name=%s and organisation=%s and organisation_id=%d";
				$params = array($case["name"], $case["organisation"], $this->organisation_id);

				if (isset($case["id"])) {
					$query .= " and id!=%d";
					array_push($params, $case["id"]);
				}

				if (($result = $this->db->execute($query, $params)) === false) {
					return false;
				}
				if ($result[0]["count"] > 0) {
					$this->view->add_message($this->language->module_text("error_case_exists"));
					$result = false;
				}
			}

			return $result;
		}

		public function create_case($case) {
			$keys = array("id", "organisation_id", "standard_id", "name", "organisation", "date", "scope", "impact", "interests", "logo", "archived");

			$case["id"] = null;
			$case["organisation_id"] = $this->organisation_id;
			$case["interests"] = "";
			if (trim($case["logo"]) == "") {
				$case["logo"] = null;
			}
			$case["archived"] = NO;

			$this->encrypt($case, "name", "organisation", "scope", "impact");

			return $this->db->insert("cases", $case, $keys);
		}

		public function update_case($case) {
			$keys = array("standard_id", "name", "organisation", "date", "scope", "impact", "logo", "archived");

			if (trim($case["logo"]) == "") {
				$case["logo"] = null;
			}
			$case["archived"] = is_true($case["archived"] ?? false) ? YES : NO;

			$this->encrypt($case, "name", "organisation", "scope", "impact");

			return $this->db->update("cases", $case["id"], $case, $keys);
		}

		public function delete_case($case_id) {
			$queries = array(
				array("delete from case_progress where case_id=%d", $case_id),
				array("delete from case_scenarios where case_id=%d", $case_id),
				array("delete from case_risk_control where case_risk_id in (select id from case_risks where case_id=%d)", $case_id),
				array("delete from case_risk_bia where case_risk_id in (select id from case_risks where case_id=%d)", $case_id),
				array("delete from case_risks where case_id=%d", $case_id),
				array("delete from case_scope where case_id=%d", $case_id),
				array("delete from cases where id=%d", $case_id));

			return $this->db->transaction($queries);
		}
	}
?>
