<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class setup_model extends Banshee\model {
		private $required_php_extensions = array("gd", "libxml", "mysqli", "xsl");

		/* Determine next step
		 */
		public function step_to_take() {
			$missing = $this->missing_php_extensions();
			if (count($missing) > 0) {
				return "php_extensions";
			}

			if ($this->db->connected == false) {
				$db = new \Banshee\Database\MySQLi_connection(DB_HOSTNAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
			} else {
				$db = $this->db;
			}

			if ($db->connected == false) {
				/* No database connection
				 */
				if ((DB_HOSTNAME == "localhost") && (DB_DATABASE == "ravib") && (DB_USERNAME == "ravib") && (DB_PASSWORD == "ravib")) {
					return "db_settings";
				} else if (strpos(DB_PASSWORD, "'") !== false) {
					$this->view->add_system_message("A single quote is not allowed in the password!");
					return "db_settings";
				}

				return "create_db";
			}

			$result = $db->execute("show tables like %s", "settings");
			if (count($result) == 0) {
				return "import_sql";
			}

			if ($this->settings->database_version < $this->latest_database_version()) {
				return "update_db";
			}

			$result = $db->execute("select password from users where username=%s", "admin");
			if ($result[0]["password"] == "none") {
				return "credentials";
			}

			return "done";
		}

		/* Missing PHP extensions
		 */
		public function missing_php_extensions() {
			static $missing = null;

			if ($missing !== null) {
				return $missing;
			}

			$missing = array();
			foreach ($this->required_php_extensions as $extension) {
				if (extension_loaded($extension) == false) {
					array_push($missing, $extension);
				}
			}

			return $missing;
		}

		/* Remove datase related error messages
		 */
		public function remove_database_errors() {
			$errors = explode("\n", rtrim(ob_get_contents()));
			ob_clean();

			foreach ($errors as $error) {
				if (strpos(strtolower($error), "mysqli_connect") === false) {
					print $error."\n";
				}
			}
		}

		/* Create the MySQL database
		 */
		public function create_database($username, $password) {
			$db = new \Banshee\Database\MySQLi_connection(DB_HOSTNAME, "mysql", $username, $password);

			if ($db->connected == false) {
				$this->view->add_message("Error connecting to database.");
				return false;
			}

			$db->query("begin");

			/* Create database
			 */
			$query = "create database if not exists %S character set utf8";
			if ($db->query($query, DB_DATABASE) == false) {
				$db->query("rollback");
				$this->view->add_message("Error creating database.");
				return false;
			}

			/* Create user
			 */
			$query = "select count(*) as count from user where User=%s";
			if (($users = $db->execute($query, DB_USERNAME)) === false) {
				$db->query("rollback");
				$this->view->add_message("Error checking for user.");
				return false;
			}

			if ($users[0]["count"] == 0) {
				$query = "create user %s@%s identified by %s";
				if ($db->query($query, DB_USERNAME, DB_HOSTNAME, DB_PASSWORD) == false) {
					$db->query("rollback");
					$this->view->add_message("Error creating user.");
					return false;
				}
			}

			/* Set access rights
			 */
			$rights = array(
				"select", "insert", "update", "delete",
				"create", "drop", "alter", "index", "lock tables",
				"create view", "show view");

			$query = "grant ".implode(", ", $rights)." on %S.* to %s@%s";
			if ($db->query($query, DB_DATABASE, DB_USERNAME, DB_HOSTNAME) == false) {
				$db->query("rollback");
				$this->view->add_message("Error setting access rights.");
				return false;
			}

			/* Test access
			 */
			if ($users[0]["count"] > 0) {
				$login_test = new \Banshee\Database\MySQLi_connection(DB_HOSTNAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
				if ($login_test->connected == false) {
					$db->query("rollback");
					$this->view->add_message("Invalid credentials in settings/banshee.conf.");
					return false;
				}
			}

			/* Commit changes
			 */
			$db->query("commit");
			$db->query("flush privileges");
			unset($db);

			return true;
		}

		/* Import database tables from file
		 */
		public function import_sql() {
			if (($queries = file("../database/mysql.sql")) === false) {
				$this->view->add_message("Can't read the database/mysql.sql file.");
				return false;
			}

			if (($db_link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE)) === false) {
				$this->view->add_message("Error while connecting to the database.");
				return false;
			}

			$query = "";
			foreach ($queries as $line) {
				if (($line = trim($line)) == "") {
					continue;
				}
				if (substr($line, 0, 2) == "--") {
					continue;
				}

				$query .= $line;
				if (substr($query, -1) == ";") {
					if (mysqli_query($db_link, $query) === false) {
						$this->view->add_message("Error while executing query [%s].", $query);
						return false;
					}
					$query = "";
				}
			}

			mysqli_close($db_link);

			$this->db->query("update users set status=%d", USER_STATUS_CHANGEPWD);
			$this->settings->secret_website_code = random_string(32);

			return true;
		}

		/* Collect latest database version from update_database() function
		 */
		private function latest_database_version() {
			$old_db = $this->db;
			$old_settings = $this->settings;
			$this->db = new dummy_object();
			$this->settings = new dummy_object();
			$this->settings->database_version = 1;

			$this->update_database();
			$version = $this->settings->database_version;

			unset($this->db);
			unset($this->settings);
			$this->db = $old_db;
			$this->settings = $old_settings;

			return $version;
		}

		/* Execute query and report errors
		 */
		private function db_query($query) {
			static $first = true;
			static $logfile = null;

			$args = func_get_args();
			array_shift($args);

			if ($this->db->query($query, $args) === false) {
				if ($first) {
					$this->view->add_message("The following queries failed (also added to debug logfile):");
					$first = false;
				}

				$query = str_replace("%s", "'%s'", $query);
                $query = str_replace("%S", "`%s`", $query);
				$query = vsprintf($query, $args);
				$this->view->add_message(" - %s", $query);

				if ($logfile === null) {
					$logfile = new \Banshee\logfile("debug");
				}

				$logfile->add_entry("Failed query: %s", $query);
			}
		}

		/* Update database
		 */
		public function update_database() {
			if ($this->settings->database_version == 1) {
				$this->db_query("ALTER TABLE case_threats ADD actor_id INT UNSIGNED NULL AFTER threat");
				$this->db_query("ALTER TABLE case_threats ADD INDEX(actor_id)");
				$this->db_query("ALTER TABLE case_threats ADD FOREIGN KEY (actor_id) REFERENCES actors(id) ".
				                 "ON DELETE RESTRICT ON UPDATE RESTRICT");
				$this->db_query("ALTER TABLE cases ADD interests TEXT NOT NULL AFTER impact");

				$this->settings->database_version = 2;
			}

			if ($this->settings->database_version == 2) {
				$this->db_query("CREATE TABLE case_scenarios (id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                 "case_id int(10) unsigned NOT NULL, title text NOT NULL, scenario text NOT NULL, ".
				                 "consequences text NOT NULL, PRIMARY KEY (id), KEY case_id (case_id), ".
				                 "CONSTRAINT case_scenarios_ibfk_1 FOREIGN KEY (case_id) REFERENCES cases (id)) ".
				                 "ENGINE=InnoDB DEFAULT CHARSET=utf8");

				$this->settings->database_version = 3;
			}

			if ($this->settings->database_version == 3) {
				$this->db_query("ALTER TABLE case_threats ADD causes TEXT NOT NULL AFTER handle");
				$this->db_query("ALTER TABLE case_threats ADD effects TEXT NOT NULL AFTER causes");

				$this->settings->database_version = 4;
			}

			if ($this->settings->database_version == 4) {
				$this->db_query("RENAME TABLE case_threat_measure TO case_risk_control");
				$this->db_query("RENAME TABLE case_threat_bia TO case_risk_bia");
				$this->db_query("RENAME TABLE case_threats TO case_risks");
				$this->db_query("RENAME TABLE measures TO controls");
				$this->db_query("RENAME TABLE measure_categories TO control_categories");
				$this->db_query("RENAME TABLE standards TO control_standards");

				$this->db_query("ALTER TABLE controls CHANGE reduce reduces TINYINT(3) UNSIGNED NOT NULL");
				$this->db_query("ALTER TABLE case_risk_bia CHANGE case_threat_id case_risk_id INT(10) UNSIGNED NOT NULL");
				$this->db_query("ALTER TABLE case_risk_control CHANGE case_threat_id case_risk_id INT(10) UNSIGNED NOT NULL");
				$this->db_query("ALTER TABLE case_risk_control CHANGE measure_id control_id INT(10) UNSIGNED NOT NULL");
				$this->db_query("ALTER TABLE case_progress CHANGE measure_id control_id INT(10) UNSIGNED NULL DEFAULT NULL");
				$this->db_query("ALTER TABLE mitigation CHANGE measure_id control_id INT(10) UNSIGNED NOT NULL");

				$this->db_query("ALTER TABLE languages CHANGE page module VARCHAR(50) NOT NULL");
				$this->db_query("ALTER TABLE languages ADD javascript BOOLEAN NOT NULL AFTER name");

				$this->db_query("DROP TABLE log_page_views, log_referers, log_visits, risk_assess_values, risk_assess_sessions");

				$this->db_query("ALTER TABLE bia CHANGE location location TINYTEXT NOT NULL");
				$this->db_query("UPDATE bia SET location=%d WHERE location=%s", 0, "intern");
				$this->db_query("UPDATE bia SET location=%d WHERE location=%s", 1, "extern");
				$this->db_query("UPDATE bia SET location=%d WHERE location=%s", 2, "saas");
				$this->db_query("ALTER TABLE bia CHANGE location location TINYINT UNSIGNED NOT NULL");

				$this->db_query("ALTER TABLE roles DROP %S", "casus/risico");

				$modules = array(
					"actoren"                    => "actors",
					"adviseur"                   => "advisor",
					"casus"                      => "case",
					"casus/scope"                => "case/scope",
					"casus/belangen"             => "case/interests",
					"casus/dreigingen"           => "case/risks",
					"casus/maatregelen"          => "case/controls",
					"casus/scenarios"            => "case/scenarios",
					"casus/rapportage"           => "case/report",
					"casus/voortgang"            => "case/progress",
					"casus/voortgang/rapportage" => "case/progress/report",
					"casus/voortgang/export"     => "case/progress/export",
					"casus/voortgang/gereed"     => "case/progress/done",
					"cms/language"               => "cms/translation",
					"cms/measures"               => "cms/control",
					"cms/measures/categories"    => "cms/control/category",
					"cms/measures/export"        => "cms/control/export",
					"cms/standards"              => "cms/standard",
					"cms/threats"                => "cms/threat",
					"cms/threats/categories"     => "cms/threat/category",
					"cms/threats/export"         => "cms/threat/export");

				foreach ($modules as $from => $to) {
					$this->db_query("ALTER TABLE roles CHANGE %S %S TINYINT(4) NULL DEFAULT %d", $from, $to, 0);
				}

				$this->db_query("ALTER TABLE controls CHANGE name name_en VARCHAR(100) NOT NULL");
				$this->db_query("ALTER TABLE controls ADD name_nl VARCHAR(100) NOT NULL AFTER name_en");
				$this->db_query("UPDATE controls SET name_nl=name_en");

				$this->db_query("ALTER TABLE control_categories CHANGE name name_en VARCHAR(100) NOT NULL");
				$this->db_query("ALTER TABLE control_categories ADD name_nl VARCHAR(100) NOT NULL AFTER name_en");
				$this->db_query("UPDATE control_categories SET name_nl=name_en");

				$this->db_query("ALTER TABLE threats CHANGE threat threat_en VARCHAR(100) NOT NULL, ".
				                                    "CHANGE description description_en TEXT NOT NULL");
				$this->db_query("ALTER TABLE threats ADD threat_nl VARCHAR(100) NOT NULL AFTER description_en, ".
				                                    "ADD description_nl TEXT NOT NULL AFTER threat_nl");
				$this->db_query("UPDATE threats SET threat_nl=threat_en, description_nl=description_en");

				$this->db_query("ALTER TABLE threat_categories CHANGE name name_en VARCHAR(100) NOT NULL");
				$this->db_query("ALTER TABLE threat_categories ADD name_nl VARCHAR(100) NOT NULL AFTER name_en");
				$this->db_query("UPDATE threat_categories SET name_nl=name_en");

				$this->settings->database_version = 5;
			}

			return true;
		}

		/* Import INSERTs from mysql.sql
		 */
		public function import_inserts($tables) {
			if (count($tables) == 0) {
				return;
			}

			if (($queries = file("../database/mysql.sql")) === false) {
				$this->view->add_message("Can't read the database/mysql.sql file.");
				return false;
			}

			if (($db_link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE)) === false) {
				$this->view->add_message("Error while connecting to the database.");
				return false;
			}

			foreach ($tables as $table) {
				mysqli_query($db_link, "DELETE FROM `".$table."`");
				mysqli_query($db_link, "ALTER TABLE `".$table."` AUTO_INCREMENT=1");
			}

			$line = "";
			foreach ($queries as $part) {
				if (($part = trim($part)) == "") {
					continue;
				}

				if (substr($part, 0, 2) == "--") {
					continue;
				}

				$line .= $part;
				if (substr($part, -1) != ";") {
					continue;
				}

				$query = $line;
				$line = "";

				if (substr($query, 0, 6) != "INSERT") {
					continue;
				}

				list(, $table) = explode("`", $query, 3);
				if (in_array($table, $tables) == false) {
					continue;
				}

				if (mysqli_query($db_link, $query) === false) {
					$this->view->add_message("Error while executing query [%s].", $query);
					return false;
				}
			}

			mysqli_close($db_link);
		}

		/* Set administrator password
		 */
		public function set_admin_credentials($username, $password, $repeat) {
			$result = true;

			if (valid_input($username, VALIDATE_NONCAPITALS.VALIDATE_NUMBERS."@.-_", VALIDATE_NONEMPTY) == false) {
				$this->view->add_message("The username must consist of lowercase letters or an e-mail address.");
				$result = false;
			}

			if ($password != $repeat) {
				$this->view->add_message("The passwords do not match.");
				$result = false;
			}

			if (is_secure_password($password, $this->view, $this->language) == false) {
				$result = false;
			}

			if ($result == false) {
				return false;
			}

			$crypto_key = random_string(32);
			$aes = new \Banshee\Protocol\AES256($password);
			$crypto_key = $aes->encrypt($crypto_key);

			$password = password_hash($password, PASSWORD_ALGORITHM);

			$query = "update users set username=%s, password=%s, status=%d, crypto_key=%s where username=%s";
			if ($this->db->query($query, $username, $password, USER_STATUS_ACTIVE, $crypto_key, "admin") === false) {
				$this->view->add_message("Error while setting password.");
				return false;
			}

			return true;
		}
	}

	class dummy_object {
		private $cache = array();

		public function __set($key, $value) {
			$this->cache[$key] = $value;
		}

		public function __get($key) {
			return $this->cache[$key];
		}

		public function __call($func, $args) {
			 return true;
		}
	}
?>
