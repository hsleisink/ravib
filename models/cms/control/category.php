<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_control_category_model extends Banshee\tablemanager_model {
		protected $table = "control_categories";
		protected $order = "number";
		protected $elements = array(	
			"standard_id" => array(
				"label"    => "Standard",
				"type"     => "integer",
				"overview" => false,
				"required" => true,
				"readonly" => true),
			"number" => array(
				"label"    => "Number",
				"type"     => "integer",
				"overview" => true,
				"required" => true));

		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);
			
			$name = array(
				"label"    => null,
				"type"     => "varchar",
				"overview" => null,
				"required" => true);

			foreach (config_array(SUPPORTED_LANGUAGES) as $code => $language) {
				$name["label"] = "Name in ".$language;
				$name["overview"] = ($code == $this->view->language);
				$this->elements["name_".$code] = $name;
			}
		}

		public function get_standard($standard) {
			return $this->borrow("cms/standard")->get_item($standard);
		}

		public function get_items() {
			$query = "select * from %S where standard_id=%d order by %S";

			return $this->db->execute($query, $this->table, $_SESSION["standard"], $this->order);
		}

		public function create_item($item) {
			$item["standard_id"] = $_SESSION["standard"];

			parent::create_item($item);
		}
	}
?>
