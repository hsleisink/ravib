<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_control_export_model extends Banshee\model {
		public function get_categories($standard) {
			$query = "select id, standard_id, number %S as name ".
			         "from control_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, "name_".$this->view->language, $standard)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category;
			}

			return $result;
		}

		public function get_controls($standard) {
			$query = "select * from controls where standard_id=%d order by id";

			return $this->db->execute($query, $standard);
		}
	}
?>
