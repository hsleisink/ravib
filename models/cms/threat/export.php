<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_threat_export_model extends Banshee\model {
		public function get_categories() {
			$query = "select id, %S as name from threat_categories order by name";

			if (($categories = $this->db->execute($query, "name_".$this->view->language)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["id"]] = $category;
			}

			return $result;
		}

		public function get_threats($standard) {
			$query = "select t.*,(select count(*) from mitigation m, controls c ".
					 "where m.control_id=c.id and standard_id=%d and threat_id=t.id) as links from threats t ".
					 "order by number";

			return $this->db->execute($query, $standard);
		}

		public function get_mitigation($threat_id) {
			$query = "select c.number from controls c, mitigation m ".
			         "where c.id=m.control_id and m.threat_id=%d";

			return $this->db->execute($query, $threat_id);
		}
	}
?>
