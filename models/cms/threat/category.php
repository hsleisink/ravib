<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_threat_category_model extends Banshee\tablemanager_model {
		protected $table = "threat_categories";
		protected $order = "";
		protected $elements = array();

		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);

			$this->order = "name_".$this->view->language;

			$name = array(
				"label"	=> null,
				"type"	 => "varchar",
				"overview" => null,
				"required" => true);

			foreach (config_array(SUPPORTED_LANGUAGES) as $code => $language) {
				$name["label"] = "Name in ".$language;
				$name["overview"] = ($code == $this->view->language);
				$this->elements["name_".$code] = $name;
			}
		}

		public function delete_oke($item_id) {
			$result = true;

			$query = "select count(*) as count from threats where category_id=%d";
			if (($threats = $this->db->execute($query, $item_id)) === false) {
				$this->view->add_message("Error counting associated threats.");
				$result = false;
			} else if ($threats[0]["count"] > 0) {
				$this->view->add_message("Category in use.");
				$result = false;
			}

			return $result;
		}
	}
?>
