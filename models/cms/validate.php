<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_validate_model extends Banshee\model {
		public function get_standard($standard) {
			return $this->borrow("cms/standard")->get_item($standard);
		}

		public function linked_threats($standard) {
			$query = "select id, number, %S as threat, (select count(*) from mitigation m, controls c ".
			                   "where threat_id=t.id and m.control_id=c.id and c.standard_id=%d) as links ".
			         "from threats t order by links,number";
			if (($threats = $this->db->execute($query, "threat_".$this->view->language, $standard)) === false) {
				return false;
			}

			return $threats;
		}

		public function linked_controls($standard) {
			$query = "select id, number, %S as name, (select count(*) from mitigation where control_id=m.id) as links ".
			         "from controls m where standard_id=%d order by links,id";
			if (($controls = $this->db->execute($query, "name_".$this->view->language, $standard)) === false) {
				return false;
			}

			return $controls;
		}
	}
?>
