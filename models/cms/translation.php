<?php
	class cms_translation_model extends Banshee\model {
		private $columns = array();

        public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);

			foreach ($this->language->supported as $code => $language) {
				array_push($this->columns, $code);
			}
			array_unshift($this->columns, "module", "name", "javascript");
		}

		public function __get($key) {
			switch ($key) {
				case "modules": return $this->modules;
			}

			return null;
		}

		public function get_translations($module) {
			$query = "select * from languages where module=%s order by name";

			return $this->db->execute($query, $module);
		}

		public function get_translation($translation_id) {
			return $this->db->entry("languages", $translation_id);
		}

		public function save_okay($translation) {
			$result = true;

			if (valid_input($translation["name"], VALIDATE_NONCAPITALS.VALIDATE_NUMBERS."_", VALIDATE_NONEMPTY) == false) {
				$this->view->add_message("Invalid name.");
				$result = false;
			} else if (valid_input(substr($translation["name"], 0, 1), VALIDATE_NONCAPITALS, VALIDATE_NONEMPTY) == false) {
				$this->view->add_message("Invalid name.");
				$result = false;
			}

			/* Check if name is unique
			 */
			$query = "select count(*) as count from languages where module=%s and name=%s";
			$args = array($translation["module"], $translation["name"]);

			if (isset($translation["id"])) {
				$query .= " and id!=%d";
				array_push($args, $translation["id"]);
			}

			if (($count = $this->db->execute($query, $args)) === false) {
				$this->view->add_message("Database error.");
				return false;
			}

			if ($count[0]["count"] > 0) {
				$this->view->add_message("This translation has already been defined.");
				$result = false;
			}

			return $result;
		}

		public function create_translation($translation) {
			$keys = $this->columns;
			array_unshift($keys, "id");

			$translation["id"] = null;
			$translation["javascript"] = is_true($translation["javascript"] ?? false) ? YES : NO;

			return $this->db->insert("languages", $translation, $keys);
		}

		public function update_translation($translation) {
			$translation["javascript"] = is_true($translation["javascript"] ?? false) ? YES : NO;

			return $this->db->update("languages", $translation["id"], $translation, $this->columns);
		}

		public function delete_translation($translation_id) {
			return $this->db->delete("languages", $translation_id);
		}
	}
?>
