<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_controls_model extends ravib_model {
		public function get_case_risks($case_id) {
			$query = "select r.*, (select count(*) from case_risk_control s, cases c, controls m ".
			         "where s.case_risk_id=r.id and r.case_id=c.id and s.control_id=m.id ".
			         "and c.standard_id=m.standard_id) as controls from case_risks r where case_id=%d";

			if (($risks = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($risks) as $key) {
				$this->decrypt($risks[$key], "threat", "action", "current", "argumentation");
			}

			uasort($risks, function($risk1, $risk2) {
				return strcmp($risk1["threat"], $risk2["threat"]);
			});

			return $risks;
		}

		public function get_case_risk($risk_id, $case_id) {
			return $this->borrow("case/risks")->get_case_risk($risk_id, $case_id);
		}

		public function get_selected_controls($threat_id, $case_id) {
			$query = "select s.* from case_risk_control s, case_risks r, cases c, controls m ".
			         "where s.case_risk_id=r.id and r.id=%d and r.case_id=c.id and c.id=%d ".
			         "and s.control_id=m.id and m.standard_id=c.standard_id";

			if (($selected = $this->db->execute($query, $threat_id, $case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($selected as $item) {
				array_push($result, $item["control_id"]);
			}

			return $result;
		}

		public function get_controls_standard($standard_id) {
			$query = "select id, standard_id, number, %S as name ".
			         "from control_categories where standard_id=%d order by number";
			if (($result = $this->db->execute($query, "name_".$this->view->language, $standard_id)) === false) {
				return false;
			}

			$categories = array();
			foreach ($result as $item) {
				$categories[$item["number"]] = $item;
			}

			$query = "select id, standard_id, number, %S as name, reduces ".
			         "from controls where standard_id=%d order by id";
			if (($controls = $this->db->execute($query, "name_".$this->view->language, $standard_id, ".")) === false) {
				return false;
			}

			$category = 0;
			foreach ($controls as $m => $control) {
				list($number) = explode(".", $control["number"], 2);
				if ($number != $category) {
					$category = $number;
					$controls[$m]["category"] = $categories[$category] ?? null;
				}
			}

			return $controls;
		}

		public function get_controls_threat($threat_id) {
			$query = "select * from mitigation where threat_id=%d";

			return $this->db->execute($query, $threat_id);
		}

		public function get_threats() {
			$query = "select id, number, %S as threat, %S as description, ".
			         "category_id, confidentiality, integrity, availability ".
			         "from threats order by number";

			return $this->db->execute($query, "threat_".$this->view->language, "description_".$this->view->language);
		}

		public function effective_control($control_reduces, $threat_handle) {
			/* x = control_reduces: chance|impact|chance & impact
			 * y = threat_handle:  control|avoid|resist|accept
			 */
			$effective = array(
				array(YES, YES, YES),
				array(YES,  NO, YES),
				array( NO, YES, YES),
				array( NO,  NO,  NO));

			return $effective[$threat_handle - 1][$control_reduces];
		}

		public function save_controls($controls, $case) {
			if ($this->get_case_risk($controls["case_risk_id"], $case["id"]) == false) {
				return false;
			}

			$this->db->query("begin");

			$query = "delete from case_risk_control where case_risk_id=%d and control_id in ".
			         "(select id from controls where standard_id=%d)";
			if ($this->db->query($query, $controls["case_risk_id"], $case["standard_id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if (is_array($controls["selected"] ?? null)) {
				$data = array("case_risk_id" => $controls["case_risk_id"]);
				foreach ($controls["selected"] as $control_id) {
					$data["control_id"] = $control_id;
					if ($this->db->insert("case_risk_control", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
