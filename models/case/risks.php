<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_risks_model extends ravib_model {
		public function get_case_risks($case_id) {
			$query = "select * from case_risks where case_id=%d order by id";

			if (($risks = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($risks) as $key) {
				$this->decrypt($risks[$key], "threat", "causes", "effects", "action", "current", "argumentation");
			}

			uasort($risks, function($risk1, $risk2) {
				return strcmp($risk1["threat"], $risk2["threat"]);
			});

			return $risks;
		}

		public function get_categories() {
			return $this->borrow("cms/threat")->get_categories();
		}

		public function get_case_risk($risk_id, $case_id) {
			$query = "select * from case_risks where id=%d and case_id=%d";
			if (($result = $this->db->execute($query, $risk_id, $case_id)) == false) {
				return false;
			}
			$risk = $result[0];

			$query = "select bia_id from case_risk_bia where case_risk_id=%d";
			if (($result = $this->db->execute($query, $risk_id)) === false) {
				return false;
			}
			$risk["risk_scope"] = array();
			foreach ($result as $item) {
				array_push($risk["risk_scope"], $item["bia_id"]);
			}

			$this->decrypt($risk, "threat", "causes", "effects", "action", "current", "argumentation");

			return $risk;
		}

		public function get_threat_templates() {
			$query = "select id, number, %S as threat, %S as description, ".
			         "category_id, confidentiality, integrity, availability ".
			         "from threats order by number";

			return $this->db->execute($query, "threat_".$this->view->language, "description_".$this->view->language);
		}

		public function get_threat_template($threat_id, $standard_id) {
			if (($template = $this->db->entry("threats", $threat_id)) == false) {
				return false;
			}
			$template["threat"] = $template["threat_".$this->view->language];
			$template["description"] = $template["description_".$this->view->language];

			$query = "select name from control_standards where id=%d";
			if (($standard = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}
			$template["standard"] = $standard[0]["name"];

			$query = "select id, standard_id, number, %S as name, reduces from controls m, mitigation g ".
			         "where m.id=g.control_id and g.threat_id=%d and m.standard_id=%d order by m.id";
			if (($template["controls"] = $this->db->execute($query, "name_".$this->view->language, $template["id"], $standard_id)) === false) {
				return false;
			}

			return $template;
		}

		public function get_scope($case_id) {
			$query = "select b.* from bia b, case_scope s where b.id=s.bia_id and s.case_id=%d";

			if (($scope = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($scope) as $key) {
				$this->decrypt($scope[$key], "item", "description", "impact");
			}

			uasort($scope, function($bia1, $bia2) {
				return strcmp($bia1["item"], $bia2["item"]);
			});

			return $scope;
		}

		public function get_actors() {
			if (($actors = $this->borrow("actors")->get_actors()) === false) {
				return false;
			}

			foreach ($actors as $i => $actor) {
				$threat_level = $this->borrow("actors")->actor_threat($actor);
				$actors[$i]["threat"] = ACTOR_THREAT_LABELS[$threat_level];
			}

			return $actors;
		}

		public function save_okay($risk) {
			$result = true;

			if (trim($risk["threat"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_threat"));
				$result = false;
			}

			if (($risk["chance"] < 1) || ($risk["chance"] > 5)) {
				$this->view->add_message($this->language->module_text("error_select_chance"));
				$result = false;
			}

			if (($risk["impact"] < 1) || ($risk["impact"] > 5)) {
				$this->view->add_message($this->language->module_text("error_select_impact"));
				$result = false;
			}

			if (($risk["handle"] < 1) || ($risk["handle"] > 4)) {
				$this->view->add_message($this->language->module_text("error_select_approach"));
				$result = false;
			}

			return $result;
		}

		private function save_risk_scope($scope, $risk_id, $case_id) {
			if ($this->get_case_risk($risk_id, $case_id) == false) {
				return false;
			}

			$query = "delete from case_risk_bia where case_risk_id=%d";
			if ($this->db->query($query, $risk_id) === false) {
				return false;
			}

			if (is_array($scope) == false) {
				return true;
			}

			$query = "select bia_id from case_scope where case_id=%d";
			if (($result = $this->db->execute($query, $case_id)) === false) {
				return false;
			}
			$bia_scope = array();
			foreach ($result as $item) {
				array_push($bia_scope, $item["bia_id"]);
			}

			$data = array("case_risk_id" => $risk_id);
			foreach ($scope as $bia_id) {
				if (in_array($bia_id, $bia_scope)) {
					$data["bia_id"] = $bia_id;
					if ($this->db->insert("case_risk_bia", $data) === false) {
						return false;
					}
				}
			}

			return true;
		}

		public function create_risk($risk, $case) {
			$keys = array("id", "case_id", "threat", "actor_id", "chance", "impact", "handle", "causes", "effects", "action", "current", "argumentation");

			$risk["id"] = null;
			$risk["case_id"] = $case["id"];
			if ($risk["actor_id"] == 0) {
				$risk["actor_id"] = null;
			}

			$this->encrypt($risk, "threat", "causes", "effects", "action", "current", "argumentation");

			$this->db->query("begin");

			if ($this->db->insert("case_risks", $risk, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}
			$risk_id = $this->db->last_insert_id;

			if ($this->save_risk_scope($risk["risk_scope"] ?? null, $risk_id, $case["id"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			if (isset($risk["template_id"])) {
				$data = array("case_risk_id" => $this->db->last_insert_id);

				if (($template = $this->get_threat_template($risk["template_id"], $case["standard_id"])) == false) {
					$this->db->query("rollback");
					return false;
				}

				foreach ($template["controls"] as $control) {
					$data["control_id"] = $control["id"];
					if ($this->db->insert("case_risk_control", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function update_risk($risk, $case_id) {
			if ($this->get_case_risk($risk["id"], $case_id) == false) {
				return false;
			}

			if ($risk["actor_id"] == 0) {
				$risk["actor_id"] = null;
			}

			$this->encrypt($risk, "threat", "causes", "effects", "action", "current", "argumentation");

			$keys = array("threat", "actor_id", "chance", "impact", "handle", "causes", "effects", "action", "current", "argumentation");
			if ($this->db->update("case_risks", $risk["id"], $risk, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->save_risk_scope($risk["risk_scope"] ?? null, $risk["id"], $case_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_risk($risk_id, $case_id) {
			if ($this->get_case_risk($risk_id, $case_id) == false) {
				return false;
			}

			$queries = array(
				array("delete from case_risk_control where case_risk_id=%d", $risk_id),
				array("delete from case_risk_bia where case_risk_id=%d", $risk_id),
				array("delete from case_risks where id=%d", $risk_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
