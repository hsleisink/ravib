<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_progress_report_model extends ravib_model {
		private function get_mitigation($case_id) {
			$query = "select g.* from case_risk_control g, case_risks r where g.case_risk_id=r.id and r.case_id=%d";
			if (($controls = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($controls as $control) {
				$id = $control["case_risk_id"];

				if (is_array($result[$id] ?? false) == false) {
					$result[$id] = array();
				}

				array_push($result[$id], $control["control_id"]);
			}

			return $result;
		}

		private function get_progress($case_id) {
			$query = "select * from case_progress where case_id=%d";

			return $this->db->execute($query, $case_id);
		}

		public function generate_report($case) {
			if (($standard = $this->get_standard($case["standard_id"])) == false) {
				return false;
			}

			if (($risks = $this->borrow("case/risks")->get_case_risks($case["id"])) === false) {
				return false;
			}

			if (($controls = $this->borrow("case/progress")->get_case_controls($case["id"])) === false) {
				return false;
			}

			if (($control_categories = $this->borrow("case/progress")->get_control_categories($case["standard_id"])) === false) {
				return false;
			}

			if (($mitigation = $this->get_mitigation($case["id"])) === false) {
				return false;
			}

			if (($progress = $this->get_progress($case["id"])) === false) {
				return false;
			}

			foreach ($risks as $i => $risk) {
				$risks[$i]["risk_value"] = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
				$risks[$i]["relevant"] = ($risk["handle"] != RISK_ACCEPT) && ($risk["handle"] > 0);
			}

			/* Generate report
			 */
			$pdf = new RAVIB_report($case["title"]);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." and RAVIB");
			$pdf->SetSubject($this->language->module_text("subject"));
			$pdf->SetKeywords($this->language->module_text("keywords"));
			$pdf->AliasNbPages();

			/* Title
			 */
			$pdf->AddPage();
			$pdf->Bookmark($this->language->module_text("frontpage"));
			$pdf->SetFont("helvetica", "B", 16);
			$pdf->Ln(100);
			$pdf->Cell(0, 0, $this->language->module_text("title"), 0, 1, "C");
			$pdf->SetFont("helvetica", "", 12);
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $case["organisation"]." :: ".$case["name"], 0, 1, "C");
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $this->language->module_text("date_risk_analysis").": ".date_string("j F Y", strtotime($case["date"])), 0, 1, "C");
			$pdf->Ln(8);
			$pdf->Cell(0, 0, $this->language->module_text("date_report").": ".date_string("j F Y"), 0, 1, "C");
			$pdf->Image("images/layout/ravib_logo.png", 140, 20, 50, 0);

			/* Progress
			 */
			$pdf->AddPage();
			$pdf->AddChapter($this->language->module_text("progress"));
			$pdf->Ln(3);

			$pdf->Write(5, sprintf($this->language->module_text("implementation_progress"), count($controls)));
			$pdf->Ln(12);

			$today = time();
			$done = $pending = $overdue = $idle = 0;
			foreach ($controls as $i => $control) {
				if ($control["done"]) {
					$done++;
				} else if ($control["deadline"] == null) {
					$idle++;
				} else if ($control["deadline"] < $today) {
					$overdue++;
					$controls[$i]["overdue"] = true;
				} else {
					$pending++;
				}
			}

			if (($total = count($controls)) > 0) {
				$done = round(100 * $done / $total, 1);
				$overdue = round(100 * $overdue / $total, 1);
				$pending = round(100 * $pending / $total, 1);
				$idle = round(100 * $idle / $total, 1);
			} else {
				$done = $overdue = $pending = $idle = 0;
			}

			if (($delta = ($done + $overdue + $pending + $idle - 100)) != 0) {
				$values = array(
					"done" => $done,
					"overdue" => $overdue,
					"pending" => $pending,
					"idle"	=> $idle);
				arsort($values);
				$key = key($values);
				$$key -= $delta;
			}

			if ($done > 0) {
				$pdf->SetFillColor(0, 192, 0);
				$pdf->Cell(1.75 * $done, 5, $done."%", 1, 0, "C", true);
			}
			if ($pending > 0) {
				$pdf->SetFillColor(255, 192, 0);
				$pdf->Cell(1.75 * $pending, 5, $pending."%", 1, 0, "C", true);
			}
			if ($overdue > 0) {
				$pdf->SetFillColor(255, 0, 0);
				$pdf->Cell(1.75 * $overdue, 5, $overdue."%", 1, 0, "C", true);
			}
			if ($idle > 0) {
				$pdf->SetTextColor(255, 255, 255);
				$pdf->SetFillColor(0, 0, 0);
				$pdf->Cell(1.75 * $idle, 5, $idle."%", 1, 0, "C", true);
				$pdf->SetTextColor(0, 0, 0);
			}

			$planned = $invested = 0;
			foreach ($progress as $task) {
				$planned += $task["hours_planned"];
				$invested += $task["hours_invested"];
			}

			$pdf->Ln(10);
			$pdf->Write(5, $this->language->module_text("total_hours_planned").": ".$planned.". ".$this->language->module_text("total_hours_invested").": ".$invested.".");

			$pdf->Ln(20);
			$pdf->Write(5, sprintf($this->language->module_text("completed_tasks"), $standard));
			$pdf->Ln(12);

			/* Progress per chapter
			 */
			$categories = array();
			foreach ($controls as $i => $control) {
				list($section) = explode(".", $control["number"]);
				if (isset($categories[$section]) == false) {
					$categories[$section] = array(0, 0);
				}
				if ($control["done"]) {
					$categories[$section][0]++;
				}
				$categories[$section][1]++;
			}
			ksort($categories);

			foreach ($categories as $key => $category) {
				$percentage = round(100 * $category[0] / $category[1]);
				$pdf->Write(6, $key.". ".$control_categories[$key].": ".$percentage."% (".$category[0]." / ".$category[1].")");
				$pdf->Ln(6);
				if ($percentage > 0) {
					$pdf->SetFillColor(92, 92, 255);
					$pdf->Cell(1.75 * $percentage, 3, "", 1, 0, "C", true);
				}
				if ($percentage < 100) {
					$pdf->SetFillColor(255, 255, 255);
					$pdf->Cell(175 - 1.75 * $percentage, 3, "", 1, 0, "C", true);
				}
				$pdf->Ln(6);
			}

			/* Risk matrix
			 */
			$pdf->AddPage();
			$pdf->AddChapter($this->language->module_text("risk_matrices"));
			$pdf->Write(5, $this->language->module_text("original_matrix"));
			$pdf->Ln(10);

			$this->borrow("case/report")->draw_risk_matrix($pdf, $risks, true);

			foreach ($mitigation as $threat_id => $controls) {
				foreach ($progress as $item) {
					if (in_array($item["control_id"], $controls) == false) {
						continue;
					}
					if (is_true($item["done"])) {
						$controls = array_diff($controls, array($item["control_id"]));
					}
				}

				$mitigation[$threat_id]["done"] = (count($controls) == 0);
			}

			foreach ($risks as $i => $risk) {
				if (is_true($mitigation[$risk["id"]]["done"] ?? false)) {
					unset($risks[$i]);
				}
			}

			$pdf->Ln(20);
			$pdf->Write(5, $this->language->module_text("matrix_with_completed_tasks"));
			$pdf->Ln(10);

			$this->borrow("case/report")->draw_risk_matrix($pdf, $risks, true);

			return $pdf;
		}
	}
?>
