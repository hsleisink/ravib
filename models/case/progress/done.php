<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_progress_done_model extends ravib_model {
		public function get_data($code) {
			if ($code == null) {
				return false;
			}

			$padding = strlen($code) % 4;
			if ($padding > 0) {
				$padding = 4 - $padding;
				$code .= str_repeat("=", $padding);
			}

			if (($data = json_decode(base64_decode(strtr($code, "-_", "+/")), true)) == null) {
				return false;
			}

			$signature = $data["signature"];
			unset($data["signature"]);

			if ($this->borrow("case/progress")->get_signature($data) != $signature) {
				return false;
			}

			return $data;
		}

		public function get_task($control_id, $case_id) {
			$query = "select c.name, p.info, p.done, concat(m.number, %s, m.name) as control, u.fullname ".
			         "from cases c, case_progress p, controls m, users u ".
			         "where c.id=%d and c.id=p.case_id and p.control_id=%d and m.id=p.control_id and u.id=p.executor_id";
			if (($result = $this->db->execute($query, " ", $case_id, $control_id)) == false) {
				return false;
			}
			$task = $result[0];

			$this->decrypt($task, "name", "info");

			return $task;
		}

		public function mark_as_done($control_id, $case_id) {
			$query = "update case_progress set done=%d where case_id=%d and control_id=%d";

			return $this->db->query($query, YES, $case_id, $control_id) !== false;
		}

		public function send_notification($control_id, $case_id) {
			if (($case = $this->get_case($case_id)) == false) {
				return false;
			} else if (($progress = $this->borrow("case/progress")->get_progress($control_id, $case_id)) == false) {
				return false;
			} else if (($executor = $this->borrow("case/progress")->get_person($progress["executor_id"])) == false) {
				return false;
			} else if (($control = $this->db->entry("controls", $control_id)) == false) {
				return false;
			}

			$query = "select u.fullname, u.email from users u, user_role r ".
			         "where u.id=r.user_id and u.organisation_id=%d and (r.role_id=%d or r.role_id=%d)";
			if (($users = $this->db->execute($query, $case["organisation_id"], ORGANISATION_ADMIN_ROLE_ID, ADMIN_ROLE_ID)) == false) {
				return false;
			}

			if (($reviewer = $this->borrow("case/progress")->get_person($progress["reviewer_id"])) != false) {
				$reviewer = $reviewer["fullname"];
			} else {
				$reviewer = "-";
			}

			if (($message = file_get_contents("../extra/task_done.txt")) === false) {
				exit("Can't load message template.\n");
			}

			$replace = array(
				"REVIEWER"    => $reviewer,
				"CASE"        => $case["name"],
				"INFORMATION" => $progress["info"],
				"CONTROL"     => $control["number"]." ".$control["name"],
				"EXECUTOR"    => $executor["fullname"]);

			$mail = new ravib_email("Task complete for ".$case["name"], $this->settings->webmaster_email, "RAVIB");
			$mail->set_message_fields($replace);
			$mail->message($message);

			foreach ($users as $user) {
				$mail->to($user["email"], $user["fullname"]);
			}

			return $mail->send();
		}
	}
?>
