<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_progress_export_model extends ravib_model {
		private function get_progress($case_id) {
			if (($progress = $this->borrow("case/progress")->get_case_controls($case_id)) === false) {
				return false;
			}

			uasort($progress, function($item_a, $item_b) {
				return version_compare($item_a["number"], $item_b["number"]);
			});

			return $progress;
		}

		public function get_export_csv($case) {
			if (($progress = $this->get_progress($case["id"])) === false) {
				return false;
			}

			if (($standard = $this->get_standard($case["standard_id"])) === false) {
				return false;
			}

			$csv = new \Banshee\csvfile();
			$csv->add_line("#", $this->language->module_text("controls_from")." ".$standard, $this->language->global_text("urgency"),
				$this->language->module_text("assigned_to"), $this->language->module_text("deadline"), $this->language->module_text("done"),
				$this->language->module_text("hours_planned"), $this->language->module_text("hours_invested"), $this->language->module_text("information"));
			foreach ($progress as $task) {
				if ($task["deadline"] != "") {
					$task["deadline"] = date_string("j F Y", $task["deadline"]);
				}
				$task["done"] = is_true($task["done"]) ? $this->language->global_text("yes") : $this->language->global_text("no");

				$line = array($task["number"], $task["name"], $task["urgency"], $task["person"], $task["deadline"], $task["done"], $task["hours_planned"] + 0, $task["hours_invested"] + 0, $task["info"]);
				$csv->add_line($line);
			}

			return $csv;
		}
	}
?>
