<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_progress_model extends ravib_model {
		public function get_control($control_id) {
			if (($control = $this->db->entry("controls", $control_id)) == false) {
				return false;
			}

			$control["name"] = $control["name_".$this->view->language];

			return $control;
		}

		public function get_control_categories($standard_id) {
			$query = "select id, standard_id, number, %S as name ".
			         "from control_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, "name_".$this->view->language, $standard_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category["name"];
			}

			return $result;
		}

		public function get_case_controls($case_id) {
			$query = "select distinct m.id, m.standard_id, m.number, %S as name, m.reduces, ".
			                "UNIX_TIMESTAMP(p.deadline) as deadline, u.fullname as person, u.email, ".
			                "p.info, p.done, p.hours_planned, p.hours_invested ".
			         "from case_risks r, case_risk_control l, cases c, controls m ".
			         "left join case_progress p on p.case_id=%d and p.control_id=m.id ".
			         "left join users u on u.id=p.executor_id ".
			         "where r.case_id=c.id and c.id=%d and r.id=l.case_risk_id and l.control_id=m.id ".
			         "and m.standard_id=c.standard_id and r.handle!=%d and r.handle!=%d";

			if (($controls = $this->db->execute($query, "name_".$this->view->language, $case_id, $case_id, 0, RISK_ACCEPT)) === false) {
				return false;
			}

			foreach (array_keys($controls) as $key) {
				$this->decrypt($controls[$key], "info");
			}

			/* Get risks
			 */
			$query = "select r.id, r.threat, r.chance, r.impact, r.handle ".
			         "from case_risks r, case_risk_control c ".
			         "where r.id=c.case_risk_id and c.control_id=%d and r.case_id=%d";
			foreach ($controls as $m => $control) {
				if (($risks = $this->db->execute($query, $control["id"], $case_id)) === false) {
					return false;
				}

				unset($controls[$m]["case_id"]);

				$highest_risk = -1;
				foreach ($risks as $t => $risk) {
					if (($risk["chance"] == 0) || ($risk["impact"] == 0) || ($risk["handle"] == 0)) {
						continue;
					}

					$risk_value = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
					if ($risk_value >= $highest_risk) {
						$highest_risk = $risk_value;
					}
				}

				$controls[$m]["risk_value"] = $highest_risk;
				if ($highest_risk > -1) {
					$controls[$m]["urgency"] = RISK_MATRIX_LABELS[$highest_risk];
					$controls[$m]["urgency_level"] = $highest_risk;
				}

				$controls[$m]["done"] = is_true($control["done"]);
			}

			return $controls;
		}

		public function get_case_risks($control_id, $case_id) {
			$query = "select threat, handle, action, current from case_risks r, case_risk_control c ".
			         "where r.case_id=%d and r.id=c.case_risk_id and c.control_id=%d";

			if (($risks = $this->db->execute($query, $case_id, $control_id)) === false) {
				return false;
			}

			foreach (array_keys($risks) as $key) {
				$this->decrypt($risks[$key], "threat", "current", "action");
			}

			return $risks;
		}

		public function sort_controls($a, $b, $order = null, $loop = 3) {
			if ($loop-- <= 0) {
				return 0;
			}

			if ($order == null) {
				$order = $_SESSION["progress_order"];
			}

			switch ($order) {
				case "deadline":
					if ($a["deadline"] == null) {
						$a["deadline"] = "9999-12-31";
					}
					if ($b["deadline"] == null) {
						$b["deadline"] = "9999-12-31";
					}
					if (($result = strcmp($a["deadline"], $b["deadline"])) == 0) {
						$result = $this->sort_controls($a, $b, "urgency", $loop);
					}
					break;
				case "person":
					if ($a["person"] == null) {
						$a["person"] = "zzzzzzzz";
					}
					if ($b["person"] == null) {
						$b["person"] = "zzzzzzzz";
					}
					if (($result = strcmp($a["person"], $b["person"])) == 0) {
						$result = $this->sort_controls($a, $b, "deadline", $loop);
					}
					break;
				case "urgency":
					if (($result = strcmp($b["risk_value"], $a["risk_value"])) == 0) {
						$result = $this->sort_controls($a, $b, "deadline", $loop);
					}
					break;
				case "done":
					if (($result = strcmp($a["done"], $b["done"])) == 0) {
						$result = $this->sort_controls($a, $b, "urgency", $loop);
					}
					break;
				default:
					$result = version_compare($a["number"], $b["number"]);
			}

			return $result;
		}

		public function get_people() {
			$query = "select id, fullname, email from users where organisation_id=%d";

			return $this->db->execute($query, $this->organisation_id);
		}

		public function get_person($id) {
			$query = "select id, fullname, email from users where id=%d and organisation_id=%d";

			if (($users = $this->db->execute($query, $id, $this->organisation_id)) == false) {
				return false;
			}

			return $users[0];
		}

		public function get_progress($control_id, $case_id) {
			$query = "select * from case_progress where case_id=%d and control_id=%d";
			if (($result = $this->db->execute($query, $case_id, $control_id)) === false) {
				return false;
			}

			if (count($result) == false) {
				return array("control_id" => $control_id);
			} else {
				$this->decrypt($result[0], "info");
				return $result[0];
			}
		}

		public function progress_okay($progress, $case_id) {
			$result = true;

			if ($progress["executor_id"] != 0) {
				if ($this->get_person($progress["executor_id"]) == false) {
					$this->view->add_message($this->language->module_text("error_unknown_person"));
					$result = false;
				}

				if ($progress["deadline"] == null) {
					$this->view->add_message($this->language->module_text("error_specify_deadline"));
					$result = false;
				} else if (valid_date($progress["deadline"]) == false) {
					$this->view->add_message($this->language->module_text("error_invalid_deadline"));
					$result = false;
				}

				if ($progress["executor_id"] == $progress["reviewer_id"]) {
					$this->view->add_message($this->language->module_text("error_executor_reviewer_different"));
					$result = false;
				}
			} else if ($progress["deadline"] != "") {
				$this->view->add_message($this->language->module_text("error_assign_task_to_someone"));
				$result = false;
			}

			return $result;
		}

		public function save_progress($progress, $case_id) {
			$query = "select count(*) as count from case_risks r, case_risk_control c ".
			         "where r.case_id=%d and r.id=c.case_risk_id and control_id=%d";
			if (($result = $this->db->execute($query, $case_id, $progress["control_id"])) === false) {
				return false;
			}
			if ($result[0]["count"] == 0) {
				return true;
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			$query = "delete from case_progress where case_id=%d and control_id=%d";
			if ($this->db->query($query, $case_id, $progress["control_id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($progress["deadline"] == "") {
				$progress["deadline"] = null;
			}

			$progress["info"] = trim($progress["info"]);

			if (($progress["executor_id"] == 0) && ($progress["reviewer_id"] == 0) && ($progress["deadline"] == null) && ($progress["info"] == "") && (($progress["done"] ?? null) == null) && ($progress["hours_planned"] == 0) && ($progress["hours_invested"] == 0)) {
				$query = "delete from case_progress where case_id=%d and control_id=%d";
				if ($this->db->query($query, $case_id, $progress["control_id"]) === false) {
					$this->db->query("rollback");
					return false;
				}
			} else {
				if ($progress["executor_id"] == 0) {
					$progress["executor_id"] = null;
				}

				if ($progress["reviewer_id"] == 0) {
					$progress["reviewer_id"] = null;
				}

				$this->encrypt($progress, "info");

				$data = array(
					"case_id"        => $case_id,
					"executor_id"    => $progress["executor_id"],
					"reviewer_id"    => $progress["reviewer_id"],
					"control_id"     => $progress["control_id"],
					"deadline"       => $progress["deadline"],
					"info"           => $progress["info"],
					"done"           => is_true($progress["done"] ?? null) ? YES : NO,
					"hours_planned"  => $progress["hours_planned"],
					"hours_invested" => $progress["hours_invested"]);
				if ($this->db->insert("case_progress", $data) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function get_signature($data) {
			return hash_hmac("sha256", json_encode($data), $this->settings->secret_website_code);
		}

		public function send_notifications($progress, $case_id) {
			if ($progress["executor_id"] == 0) {
				return true;
			}

			if (($case = $this->get_case($case_id)) == false) {
				return false;
			} else if (($executor = $this->get_person($progress["executor_id"])) == false) {
				return false;
			}

			if (($reviewer = $this->get_person($progress["reviewer_id"])) == false) {
				$reviewer = array("name" => "-");
			}

			if (($control = $this->db->entry("controls", $progress["control_id"])) == false) {
				return false;
			}

			if (($message = file_get_contents("../extra/task_assigned.txt")) === false) {
				$this->view->add_system_warning("Can't load message template.\n");
				return false;
			}

			/* Send actor e-mail
			 */
			$replace = array(
				"NAME"        => $executor["fullname"] ?? "",
				"CASE"        => $case["name"] ?? "",
				"INFORMATION" => $progress["info"] ?? "",
				"CONTROL"     => $control["number"]." ".$control["name_nl"],
				"REVIEWER"    => $reviewer["fullname"] ?? "");

			$mail = new ravib_email("Taak ".$control["number"]." inzake ".$case["name"], $this->settings->webmaster_email, "RAVIB");
			$mail->set_message_fields($replace);
			$mail->message($message);
			$mail->send($executor["email"], $executor["name"] ?? null);

			/* Send reviewer e-mail
			 */
			if ($progress["reviewer_id"] == 0) {
				return true;
			}

			if (($message = file_get_contents("../extra/task_review.txt")) === false) {
				exit("Can't load message template.\n");
			}

			$data = array(
				"case_id"    => $case_id,
				"control_id" => $progress["control_id"]);
			$data["signature"] = $this->get_signature($data);
			$code = rtrim(strtr(base64_encode(json_encode($data)), "+/", "-_"), "=");

			$link = "https://".$_SERVER["HTTP_HOST"]."/case/progress/done/".$code;

			$replace = array(
				"NAME"        => $reviewer["fullname"],
				"CASE"        => $case["name"],
				"INFORMATION" => $progress["info"],
				"CONTROL"     => $control["number"]." ".$control["name_nl"],
				"EXECUTOR"    => $executor["fullname"],
				"LINK"        => $link);

			$mail = new ravib_email("Controle op taak ".$control["number"]." inzake ".$case["name"], "no-reply@ravib.nl", "RAVIB");
			$mail->set_message_fields($replace);
			$mail->message($message);
			$mail->send($reviewer["email"], $reviewer["fullname"]);

			return true;
		}
	}
?>
