<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_report_model extends ravib_model {
		private function get_case_risks($case_id) {
			if (($risks = $this->borrow("case/risks")->get_case_risks($case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($risks as $risk) {
				$result[$risk["id"]] = $risk;
			}

			return $result;
		}

		private function get_controls($case_id) {
			$query = "select distinct m.id, m.standard_id, m.number, %S as name, m.reduces ".
			         "from controls m, case_risk_control l, case_risks r, cases c ".
					 "where m.id=l.control_id and l.case_risk_id=r.id and r.case_id=c.id and c.id=%d ".
			         "and m.standard_id=c.standard_id order by id";

			if (($controls = $this->db->execute($query, "name_".$this->view->language, $case_id)) === false) {
				return false;
			}

			$query = "select l.* from case_risk_control l, case_risks r ".
					 "where l.case_risk_id=r.id and r.case_id=%d and control_id=%d order by r.id";
			foreach ($controls as $m => $control) {
				if (($risks = $this->db->execute($query, $case_id, $control["id"])) === false) {
					return false;
				}

				$controls[$m]["risks"] = array();
				foreach ($risks as $risk) {
					array_push($controls[$m]["risks"], $risk["case_risk_id"]);
				}
			}

			return $controls;
		}

		private function get_systems_for_risk($risk_id, $case_id) {
			$query = "select b.* from bia b, case_risk_bia t, case_scope s ".
					 "where b.id=t.bia_id and t.case_risk_id=%d and s.bia_id=b.id and s.case_id=%d";

			if (($systems = $this->db->execute($query, $risk_id, $case_id)) === false) {
				return false;
			}

			foreach ($systems as $key => $system) {
				$this->decrypt($systems[$key], "item", "description", "impact");

				$a = $system["availability"] - 1;
				$i = $system["integrity"] - 1;
				$c = $system["confidentiality"] - 1;
				$systems[$key]["value"] = ASSET_VALUE[$c][$i][$a] ?? 0;
			}

			return $systems;
		}

		private function sort_risks($risk_a, $risk_b) {
			if (($risk_a["relevant"] == false) && $risk_b["relevant"]) {
				return 1;
			} else if ($risk_a["relevant"] && ($risk_b["relevant"] == false)) {
				return -1;
			}

			if ($risk_a["risk_value"] < $risk_b["risk_value"]) {
				return 1;
			} else if ($risk_a["risk_value"] > $risk_b["risk_value"]) {
				return -1;
			}

			$systems_a = is_array($risk_a["systems"]) ? count($risk_a["systems"]) : 0;
			$systems_b = is_array($risk_b["systems"]) ? count($risk_b["systems"]) : 0;
			if ($systems_a < $systems_b) {
				return 1;
			} elseif ($systems_a > $systems_b) {
				return -1;
			}

			if (($risk_a["number"] ?? 0) > ($risk_b["number"] ?? 0)) {
				return 1;
			} else if (($risk_a["number"] ?? 0) < ($risk_b["number"] ?? 0)) {
				return -1;
			}

			return 0;
		}

		private function sort_controls($control_a, $control_b) {
			if ((($control_a["risk"] ?? "") == "") && (($control_b["risk"] ?? "") != "")) {
				return 1;
			} else if ((($control_a["risk"] ?? "") != "") && (($control_b["risk"] ?? "") == "")) {
				return -1;
			}

			if (is_false($control_a["relevant"]) && is_true($control_b["relevant"])) {
				return 1;
			} else if (is_true($control_a["relevant"]) && is_false($control_b["relevant"])) {
				return -1;
			}

			if ($control_a["urgency"] < $control_b["urgency"]) {
				return 1;
			} else if ($control_a["urgency"] > $control_b["urgency"]) {
				return -1;
			}

			if (($control_a["asset_value"] ?? 0) < ($control_b["asset_value"] ?? 0)) {
				return 1;
			} else if (($control_a["asset_value"] ?? 0) > ($control_b["asset_value"] ?? 0)) {
				return -1;
			}

			return version_compare($control_a["number"], $control_b["number"]);
		}

		private function valid_url($url) {
			$parts = explode("/", $url, 4);
			if (count($parts) < 4) {
				return false;
			}

			list($protocol,, $hostname, $path) = $parts;

			switch ($protocol) {
				case "http:": $http = new \Banshee\Protocol\HTTP($hostname); break;
				case "https:": $http = new \Banshee\Protocol\HTTPS($hostname); break;
				default: return false;
			}

			if (($result = $http->GET("/".$path)) == false) {
				return false;
			}

			if ($result["status"] != 200) {
				return false;
			}

			return true;
		}

		public function draw_risk_matrix($pdf, $risks, $relevant) {
			$matrix = array();

			foreach ($risks as $risk) {
				if (($risk["chance"] == 0) || ($risk["impact"] == 0)) {
					continue;
				} else if ($relevant == ($risk["handle"] == RISK_ACCEPT)) {
					continue;
				}

				if (is_array($matrix[$risk["chance"] - 1] ?? null) == false) {
					$matrix[$risk["chance"] - 1] = array_fill(0, count(RISK_MATRIX_IMPACT), "");
				}

				$matrix[$risk["chance"] - 1][$risk["impact"] - 1]++;
			}

			$pdf->SetFont("helvetica", "", 10);

			$cell_width = 29;
			$cell_height = 8;

			$pdf->Cell($cell_width, $cell_height);
			$pdf->Cell(150, $cell_height, $this->language->global_text("impact"), 0, 0, "C");
			$pdf->Ln();
			$pdf->Cell($cell_width, $cell_height, $this->language->global_text("chance"));
			foreach (RISK_MATRIX_IMPACT as $impact) {
				$pdf->Cell($cell_width, $cell_height, $impact, 1, 0, "C");
			}
			$pdf->Ln();
			$max_y = count(RISK_MATRIX_CHANCE) - 1;
			foreach (array_reverse(RISK_MATRIX_CHANCE) as $y => $chance) {
				$pdf->Cell($cell_width, $cell_height, $chance, 1);
				foreach (RISK_MATRIX_IMPACT as $x => $impact) {
					$level = RISK_MATRIX[$max_y - $y][$x];
					$risk = RISK_MATRIX_LABELS[$level];
					$pdf->SetColor($level);
					$pdf->Cell($cell_width, $cell_height, $matrix[$max_y - $y][$x] ?? "", 1, 0, "C", true);
				}
				$pdf->Ln();
			}

			$pdf->Ln(8);
		}

		public function generate_report($case) {
			/* Get risks
			 */
			if (($risks = $this->get_case_risks($case["id"])) === false) {
				return;
			}

			foreach ($risks as $i => $risk) {
				$risks[$i]["risk_value"] = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
				$risks[$i]["relevant"] = ($risk["handle"] != RISK_ACCEPT) && ($risk["handle"] > 0);
				$risks[$i]["systems"] = $this->get_systems_for_risk($risk["id"], $case["id"]);
			}

			if (is_true($_POST["sort_by_risk"] ?? false)) {
				uasort($risks, array($this, "sort_risks"));
			}

			$nr = 1;
			foreach ($risks as $i => $risk) {
				if ($risks[$i]["relevant"] || is_true($_POST["accepted_risks"] ?? false)) {
					$risks[$i]["number"] = $nr++;
				}
			}

			/* Get controls
			 */
			if (($controls = $this->get_controls($case["id"])) === false) {
				return;
			}

			foreach ($controls as $m => $control) {
				$controls[$m]["urgency"] = 0;
				$controls[$m]["relevant"] = false;

				foreach ($control["risks"] as $risk_id) {
					$risk = $risks[$risk_id];

					if ($risk["risk_value"] > $controls[$m]["urgency"]) {
						$controls[$m]["urgency"] = $risk["risk_value"];
					}

					if ($risk["relevant"]) {
						$controls[$m]["relevant"] = true;
					}
				}
			}

			if (is_true($_POST["sort_by_risk"] ?? false)) {
				usort($controls, array($this, "sort_controls"));
			}

			/* Get BIA items
			 */
			if (($bia_items = $this->borrow("case/scope")->get_scope($case["id"])) === false) {
				return false;
			}

			foreach ($bia_items as $i => $item) {
				$query = "select case_risk_id from case_risk_bia b, case_risks r ".
						 "where r.id=b.case_risk_id and b.bia_id=%d and r.case_id=%d";
				if (($ts = $this->db->execute($query, $item["id"], $case["id"])) === false) {
					return false;
				}

				$bia_items[$i]["risks"] = array();
				foreach ($ts as $t) {
					if (($risk = $risks[$t["case_risk_id"]]) == null) {
						return false;
					}

					if (is_false($_POST["accepted_risks"] ?? false) && ($risk["relevant"] == false)) {
						continue;
					}

					if (isset($bia_items[$i]["risks"][$risk["risk_value"]]) == false) {
						$bia_items[$i]["risks"][$risk["risk_value"]] = 1;
					} else {
						$bia_items[$i]["risks"][$risk["risk_value"]]++;
					}
				}
			}

			/* Get scenarios
			 */
			if (($scenarios = $this->borrow("case/scenarios")->get_scenarios($case["id"])) === false) {
				return false;
			}

			$ra_date = date_string("j F Y", strtotime($case["date"]));
			$ra_standard = $this->get_standard($case["standard_id"]);

			/* Generate report
			 */
			$pdf = new RAVIB_report($case["title"]);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." and RAVIB");
			$pdf->SetSubject($this->language->module_text("pdf_subject"));
			$pdf->SetKeywords($this->language->module_text("pdf_keywords"));
			$pdf->AliasNbPages();

			/* Title
			 */
			$pdf->AddPage();
			$pdf->Bookmark($this->language->module_text("pdf_front_page"));
			if ($case["logo"] != "") {
				if ($this->valid_url($case["logo"])) {
					try {
						$pdf->Image($case["logo"], 15, 20, 0, 25);
					} catch (Exception $e) {
						$pdf->SetFont("helvetica", "", 8);
						$pdf->Cell(0, 0, "[ can't load logo ]");
					}
				} else {
					$pdf->SetFont("helvetica", "", 8);
					$pdf->Cell(0, 0, "[ invalid logo url ]");
				}
			}

			$pdf->SetFont("helvetica", "B", 16);
			$pdf->SetTextColor(54, 94, 145);
			$pdf->Ln(95);
			$pdf->Cell(0, 0, $this->language->module_text("pdf_title"), 0, 1, "C");
			$pdf->SetFont("helvetica", "", 12);
			$pdf->SetTextColor(64, 64, 64);
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $case["title"], 0, 1, "C");
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $ra_date, 0, 1, "C");
			$pdf->Ln(40);
			$pdf->SetFont("helvetica", "I", 12);
			$pdf->Cell(0, 0, $this->language->module_text("pdf_confidential"), 0, 1, "C");

			/* Titel achterblad
			 */
			$pdf->AddPage();
			$pdf->Image("images/layout/ravib_logo.png", 15, 240, 40, 0);
			$pdf->SetFont("helvetica", "", 8);
			$pdf->SetTextColor(128, 128, 128);
			$pdf->Ln(228);
			$pdf->Write(5, $this->language->module_text("pdf_using_ravib"));

			$pdf->SetFont("helvetica", "", 12);
			$pdf->SetTextColor(0, 0, 0);

			/* Text
			 */
			$pdf->AddPage();

			$template = file("../extra/report_".$this->view->language.".txt");
			if (is_true($_POST["extra_info"] ?? false)) {
				$template = array_merge($template, file("../extra/report_extra_".$this->view->language.".txt"));
			}

			$text = array();
			$i = 0;
			foreach ($template as $line) {
				switch (substr($line, 0, 1)) {
					case "#":
						break 2;
					case "*":
						$text[++$i] = trim($line);
						break;
					case ">":
					case "-":
						$text[$i++] = trim($line);
						break;
					case "\n":
						$i++;
						break;
					default:
						$line = str_replace("[DATE]", $ra_date, $line);
						$line = str_replace("[ORGANISATION]", $case["organisation"], $line);
						$line = str_replace("[STANDARD]", $ra_standard, $line);
						$text[$i] = (trim($text[$i] ?? "")." ".trim($line));

				}
			}

			$new_page = true;
			$list = false;
			foreach ($text as $i => $line) {
				switch (substr($line, 0, 1)) {
					case ">":
						if ($new_page == false) {
							$pdf->Ln(4);
						}
						if ($list) {
							$pdf->Ln(3);
						}
						$pdf->AddChapter(substr($line, 2));
						$new_page = false;
						$list = false;
						break;
					case "-":
						$pdf->AddPage();
						$new_page = true;
						$list = false;
						break;
					case "*":
						list($head, $line) = explode(":", $line, 2);
						$pdf->Write(5, "» ");
						$pdf->SetFont("helvetica", "I", 10);
						$pdf->Write(5, trim(substr($head, 1)).": ");
						$pdf->SetFont("helvetica", "", 10);
						$pdf->Write(5, $line);
						$pdf->Ln(5);
						$list = true;
						break;
					default:
						if ($list) {
							$pdf->Ln(3);
						}
						$pdf->Write(5, $line);
						$pdf->Ln(8);
						$list = false;
				}
			}

			/* Scenarios
			 */
			if (count($scenarios) > 0) {
				$pdf->AddPage();
				$pdf->AddChapter($this->language->module_text("pdf_scenarios"));

				$pdf->Write(5, $this->language->module_text("pdf_scenarios_introduction"));
				$pdf->Ln(8);

				foreach ($scenarios as $scenario) {
					$pdf->SetFont("helvetica", "B", 11);
					$pdf->Write(5, $scenario["title"]);
					$pdf->Ln(5);
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Write(5, $scenario["scenario"]);

					if ($scenario["consequences"] != "") {
						$pdf->Ln(8);
						$pdf->SetFont("helvetica", "I", 10);
						$pdf->Write(5, $this->language->module_text("possible_consequences"));
						$pdf->Ln(5);
						$pdf->SetFont("helvetica", "", 10);
						$pdf->Write(5, $scenario["consequences"]);
					}

					$pdf->Ln(10);
				}
			}

			/* Risk matrix
			 */
			$pdf->AddPage();
			$pdf->AddChapter($this->language->module_text("pdf_risk_matrix"));
			$pdf->Write(5, $this->language->module_text("pdf_risk_matrix_introduction"));
			$pdf->Ln(8);
			$this->draw_risk_matrix($pdf, $risks, true);

			/* Control matrix
			 */
			$pdf->AddChapter($this->language->module_text("pdf_control_urgency_distribution"));
			$pdf->Write(5, sprintf($this->language->module_text("pdf_control_urgency_distribution_introduction"), $ra_standard));
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			$matrix = array();
			foreach ($controls as $control) {
				$highest_risk = -1;

				foreach ($risks as $risk) {
					if (in_array($risk["id"], $control["risks"]) == false) {
						continue;
					}
					if (($risk["chance"] == 0) || ($risk["impact"] == 0) || ($risk["handle"] == 0)) {
						continue;
					}
					if ($risk["relevant"] == false) {
						continue;
					}
					$risk = RISK_MATRIX[$risk["chance"] - 1][$risk["impact"] - 1];
					if ($risk >= $highest_risk) {
						$highest_risk = $risk;
					}
				}

				$matrix[$highest_risk] = ($matrix[$highest_risk] ?? 0) + 1;
			}

			foreach (RISK_MATRIX_LABELS as $i => $level) {
				$pdf->Cell(30, 8, $level, 1, 0, "C");
			}
			$pdf->Ln(8);
			foreach (RISK_MATRIX_LABELS as $i => $level) {
				$pdf->SetColor($i);
				$pdf->Cell(30, 8, sprintf("%d", $matrix[$i] ?? ""), 1, 0, "C", true);
			}
			$pdf->Ln(15);

			/* Accepted risk matrix
			 */
			if (is_true($_POST["accepted_risks"] ?? false)) {
				$pdf->AddChapter($this->language->module_text("pdf_risk_matrix_accepted"));
				$pdf->Write(5, $this->language->module_text("pdf_risk_matrix_accepted_introduction"));
				$pdf->Ln(8);
				$this->draw_risk_matrix($pdf, $risks, false);
			}

			/* Impact values
			 */
			$impact_values = json_decode($case["impact"], true);
			if ($impact_values[0] != "") {
				$pdf->AddChapter($this->language->module_text("pdf_impact_definition"));
				$pdf->Write(5, $this->language->module_text("pdf_impact_definition_introduction"));
				$pdf->Ln(8);

				foreach (RISK_MATRIX_IMPACT as $i => $impact) {
					$pdf->SetFont("helvetica", "B", 10);
					$pdf->Cell(27, 5, $impact.": ");
					$pdf->SetFont("helvetica", "", 10);
					$pdf->MultiCell(150, 5, $impact_values[$i]);
				}
			}

			/* Information systems
			 */
			if (is_true($_POST["accepted_risks"] ?? false)) {
				$pdf->Ln(10);
			} else {
				$pdf->AddPage();
			}
			$pdf->AddChapter($this->language->module_text("pdf_scope"));
			$scope = rtrim($case["scope"], ".").".";
			$pdf->Write(5, $this->language->module_text("pdf_scope_limited_to")." ".$scope);
			$pdf->Ln(10);

			$this->borrow("case/interests")->add_bia($pdf, $bia_items);
			$pdf->Ln(10);

			$pdf->Write(5, $this->language->module_text("pdf_risk_per_information_system"));
			$pdf->Ln(10);

			$pdf->Cell(75, 6, "", 0, 0);
			foreach (RISK_MATRIX_LABELS as $label) {
				$pdf->Cell(26, 6, $label, 0, 0, "C");
			}
			$pdf->Ln(6);

			foreach ($bia_items as $item) {
				if ($item["scope"] == NO) {
					continue;
				}

				$pdf->Cell(75, 6, $item["item"], 0, 0);
				foreach (RISK_MATRIX_LABELS as $i => $label) {
					$pdf->SetColor($i);
					$risk = $item["risks"][$i] ?? "";
					$pdf->Cell(26, 6, $risk, 1, 0, "C", true);
				}
				$pdf->Ln(6);
			}

			$pdf->Ln(15);

			/* Interests
			 */
			$pdf->AddPage();
			if ($case["interests"] != "") {
				$pdf->AddChapter($this->language->module_text("pdf_interests"));
				$pdf->Write(5, $case["interests"]);
				$pdf->Ln(10);
			}

			/* Actors
			 */
			$pdf->AddChapter($this->language->module_text("pdf_actors"));
			$pdf->Write(5, $this->language->module_text("pdf_actors_introduction"));
			$pdf->Ln(7);

			$this->borrow("case/interests")->add_actors($pdf, $case["id"]);

			/* Risks
			 */
			$accepted_risks = 0;
			foreach ($risks as $risk) {
				if ($risk["relevant"] == false) {
					$accepted_risks++;
				}
			}

			$pdf->AddPage();
			$pdf->AddChapter($this->language->global_text("risks"));
			$pdf->Write(5, $this->language->module_text("pdf_risk_introduction"));
			if (is_false($_POST["accepted_risks"] ?? false) && ($accepted_risks > 0)) {
				$pdf->Write(5, " ".$this->language->module_text("pdf_accepted_risks"));
			}
			$pdf->Ln(12);

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(7, 6, "", "B");
			$pdf->Cell(153, 6, $this->language->global_text("risk"), "B");
			$pdf->Cell(0, 6, $this->language->module_text("pdf_exposure"), "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			foreach ($risks as $risk) {
				if (($risk["relevant"] == false) && is_false($_POST["accepted_risks"] ?? false)) {
					continue;
				}

				$risk_label = RISK_MATRIX_LABELS[$risk["risk_value"]];

				$pdf->Cell(7, 5, $risk["number"].":");
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(150, 5, $risk["threat"]);
				$pdf->SetFont("helvetica", "", 10);

				if ($risk["relevant"]) {
					$pdf->SetColor($risk["risk_value"]);
				} else {
					$pdf->SetColor();
					$risk_label = "(".$risk_label.")";
				}

				$pdf->Cell(0, 5, $risk_label, 0, 0, "C", true);
				$pdf->Ln(5);
				$pdf->SetColor();

				$chance = RISK_MATRIX_CHANCE[$risk["chance"] - 1];
				$impact = RISK_MATRIX_IMPACT[$risk["impact"] - 1];

				if ($risk["actor_id"] != null) {
					$actor = $this->borrow("actors")->get_actor($risk["actor_id"]);

					$pdf->Cell(7, 5);
					$pdf->Write(5, $this->language->module_text("pdf_most_threatening_actor").": ".$actor["name"]);
					$pdf->Ln(5);
				}

				if ($risk["causes"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, $this->language->module_text("pdf_causes").":");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $risk["causes"]);
				}

				if ($risk["effects"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, $this->language->module_text("pdf_effects").":");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $risk["effects"]);
				}

				$pdf->Cell(7, 5);
				$pdf->Cell(50, 5, $this->language->global_text("chance").": ".$chance);
				$pdf->Cell(50, 5, $this->language->global_text("impact").": ".$impact);
				$pdf->Cell(50, 5, $this->language->global_text("approach").": ".RISK_HANDLE_LABELS[$risk["handle"] - 1]);
				$pdf->Ln();

				if ($risk["action"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, $this->language->module_text("pdf_desired_situation").":");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $risk["action"]);
				}

				if ($risk["current"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, $this->language->module_text("pdf_current_situation"));
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $risk["current"]);
				}

				if ($risk["argumentation"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, $this->language->module_text("pdf_argumentation").":");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $risk["argumentation"]);
				}

				if ($risk["systems"] != false) {
					$list = array();
					foreach ($risk["systems"] as $system) {
						array_push($list, $system["item"]);
					}

					$pdf->Cell(7, 5);
					$pdf->MultiCell(163, 5, $this->language->module_text("pdf_involved_systems").": ".implode(", ", $list));
				}

				$pdf->Ln(1);

				$nr++;
			}

			/* Controls
			 */
			$pdf->AddPage();
			$pdf->AddChapter($this->language->module_text("pdf_selected_controls"));
			$pdf->Write(5, sprintf($this->language->module_text("pdf_controls_introduction"), $ra_standard));
			$pdf->Ln(12);

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(15, 6, "", "B");
			$pdf->Cell(143, 6, $this->language->module_text("pdf_control"), "B");
			$pdf->Cell(0, 6, $this->language->module_text("pdf_urgency"), "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			foreach ($controls as $control) {
				if (is_false($control["relevant"] ?? false) && is_false($_POST["accepted_risks"] ?? false)) {
					continue;
				}

				$pdf->Cell(15, 5, $control["number"]);
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(140, 5, $control["name"]);
				$pdf->SetFont("helvetica", "", 10);

				$urgency = RISK_MATRIX_LABELS[$control["urgency"]];
				if ($control["relevant"]) {
					$pdf->SetColor($control["urgency"]);
				} else {
					$pdf->SetColor();
					$urgency = "(".$urgency.")";
				}
				$pdf->Cell(0, 5, $urgency, 0, 0, "C", true);
				$pdf->Ln(5);

				$pdf->Cell(15, 5, "");
				$pdf->Cell(100, 5, sprintf($this->language->module_text("pdf_control_reduces"), CONTROL_REDUCES[$control["reduces"]]));
				$pdf->Ln(5);

				$pdf->SetColor();
				foreach ($control["risks"] as $risk_id) {
					$risk = $risks[$risk_id];

					if (is_false($_POST["accepted_risks"] ?? false)) {
						if ($risk["relevant"] == false) {
							continue;
						}
						if (($risk["chance"] == 0) || ($risk["impact"] == 0) || ($risk["handle"] == 0)) {
							continue;
						}
					}

					$pdf->Cell(15, 5, "");
					$risk_value = RISK_MATRIX_LABELS[$risk["risk_value"]];
					$handle = RISK_HANDLE_LABELS[$risk["handle"] - 1];

					$line = sprintf("%s: %s [ %s, %s ]", $risk["number"], $risk["threat"], $risk_value, $handle);
					$pdf->MultiCell(150, 5, $line);
				}
				$pdf->Ln(2);
			}

			return $pdf;
		}
	}
?>
