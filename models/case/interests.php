<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class case_interests_model extends ravib_model {
		public function save_interests($interests, $case_id) {
			$data = array("interests" => $interests);

			$this->encrypt($data, "interests");

			return $this->db->update("cases", $case_id, $data) !== false;
		}

		public function add_bia(&$pdf, $bia_items) {
			$locations = array_keys(ASSET_LOCATIONS);

			foreach ($bia_items as $nr => $item) {
				if ($item["scope"] == NO) {
					continue;
				}

				$pdf->SetFillColor(232, 232, 232);
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(0, 5, $item["item"], 0, 0, "", true);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Ln(5);

				if ($item["description"] != "") {
					$pdf->SetLeftMargin(19);
					$pdf->Write(5, $item["description"]);
					$pdf->SetLeftMargin(15);
					$pdf->Ln(5);
				}
				$pdf->SetFont("helvetica", "", 9);
				$pdf->Cell(4, 5);
				$pdf->Cell(24, 5, $this->language->module_text("pdf_a").": ".(AVAILABILITY_SCORE[$item["availability"] - 1] ?? ""));
				$pdf->Cell(23, 5, $this->language->module_text("pdf_i").": ".(INTEGRITY_SCORE[$item["integrity"] - 1] ?? ""));
				$pdf->Cell(28, 5, $this->language->module_text("pdf_c").": ".(CONFIDENTIALITY_SCORE[$item["confidentiality"] - 1] ?? ""));
				$pdf->Cell(35, 5, $this->language->global_text("value").": ".ASSET_VALUE_LABELS[$item["value"]]);
				$pdf->Cell(17, 5, $this->language->module_text("pdf_pii").": ".$this->language->global_text(is_true($item["personal_data"]) ? "yes" : "no"));
				$pdf->Cell(23, 5, $this->language->module_text("pdf_owner").": ".$this->language->global_text(is_true($item["owner"]) ? "yes" : "no"));
				$pdf->Cell(0, 5, $this->language->module_text("pdf_location").": ".$locations[$item["location"]]);
				$pdf->Ln(5);
				$pdf->SetFont("helvetica", "", 10);

				$pdf->AddTextBlock($this->language->module_text("pdf_incident_impact"), $item["impact"]);
				$pdf->Ln(1);
			}

			return true;
		}

		public function add_actors(&$pdf, $case_id) {
			if (($actors = $this->borrow("actors")->get_actors()) === false) {
				return false;
			}

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(55, 6, $this->language->module_text("pdf_name"), "B");
			$pdf->Cell(44, 6, $this->language->module_text("pdf_willingness_chance"), "B");
			$pdf->Cell(26, 6, $this->language->module_text("pdf_knowledge"), "B");
			$pdf->Cell(25, 6, $this->language->module_text("pdf_resources"), "B");
			$pdf->Cell(30, 6, $this->language->module_text("pdf_threat"), "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);
			foreach ($actors as $actor) {
				$pdf->Cell(55, 5, $actor["name"]);
				$pdf->Cell(44, 5, ACTOR_CHANCE[$actor["chance"] - 1]);
				$pdf->Cell(26, 5, ACTOR_KNOWLEDGE[$actor["knowledge"] - 1]);
				$pdf->Cell(25, 5, ACTOR_RESOURCES[$actor["resources"] - 1]);
				$threat_level = $this->borrow("actors")->actor_threat($actor);
				$pdf->Cell(30, 5, ACTOR_THREAT_LABELS[$threat_level]);
				$pdf->Ln(5);
				if (trim($actor["reason"]) != "") {
					$pdf->Cell(5, 5, "");
					$pdf->MultiCell(175, 5, $this->language->module_text("pdf_reason").": ".$actor["reason"]);
					$pdf->Ln(2);
				}
			}
			$pdf->Ln(8);

			return true;
		}

		private function capitalize_first_letter($word) {
			$word[0] = strtoupper($word[0]);

			return $word;
		}

		public function make_handout($case) {
			if (($bia_items = $this->borrow("case/scope")->get_scope($case["id"])) === false) {
				return false;
			}

			if (($templates = $this->borrow("case/risks")->get_threat_templates()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->borrow("case/risks")->get_categories()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$pdf = new RAVIB_report($case["title"], true);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." en RAVIB");
			$pdf->SetSubject($this->language->module_text("pdf_subject"));
			$pdf->SetKeywords($this->language->module_text("pdf_keywords"));
			$pdf->AliasNbPages();

			/* Information systems
			 */
			$pdf->AddPage();
			$pdf->SetFont("helvetica", "B", 15);
			$pdf->Cell(0, 5, $this->language->module_text("pdf_handout_for")." ".$case["name"], 0, 1, "C");
			if ($case["interests"] != "") {
				$pdf->Ln(8);
				$pdf->AddChapter($this->language->module_text("interests"));
				$pdf->Write(5, $case["interests"]);
			}
			$pdf->Ln(12);

			$pdf->AddChapter($this->language->module_text("pdf_bia_overview"));
			$pdf->Ln(3);
			$this->add_bia($pdf, $bia_items);

			/* Actors
			 */
			$pdf->AddPage();
			$pdf->AddChapter($this->language->module_text("pdf_actors"));

			$this->add_actors($pdf, $case["id"]);

			/* Chance
			 */
			$pdf->AddChapter($this->language->module_text("pdf_determining_chance"));
			$pdf->Write(5, $this->language->module_text("pdf_chance_rule"));
			$pdf->Ln(7);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, "", "B");
			$pdf->Cell(45, 5, $this->language->module_text("pdf_actor_threat"), "B");
			$pdf->Cell(95, 5, $this->language->module_text("pdf_frequency"), "B");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "", 10);
			for ($i = count(RISK_MATRIX_CHANCE) - 1; $i>= 0; $i--) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(40, 5, $this->capitalize_first_letter(RISK_MATRIX_CHANCE[$i]).": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Cell(45, 5, ACTOR_THREAT_LABELS[$i]);
				$pdf->Cell(95, 5, CHANCE_FREQUENCY[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Impact values
			 */
			$pdf->AddChapter($this->language->module_text("pdf_impact_interpretation"));
			$impact_values = json_decode($case["impact"], true);
			for ($i = count(RISK_MATRIX_IMPACT) - 1; $i>= 0; $i--) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(40, 5, $this->capitalize_first_letter(RISK_MATRIX_IMPACT[$i]).": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Write(5, $impact_values[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Action
			 */
			$pdf->AddChapter($this->language->module_text("pdf_approaches"));
			for ($i = 0; $i < 4; $i++) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(40, 5, $this->capitalize_first_letter(RISK_HANDLE_LABELS[$i]).":");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Write(5, $this->language->module_text("pdf_approach_".$i));
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Scales
			 */
			$pdf->AddChapter($this->language->module_text("pdf_bia_actor_scales"));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, $this->language->module_text("pdf_availability").":");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", AVAILABILITY_SCORE));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, $this->language->module_text("pdf_integrity").":");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", INTEGRITY_SCORE));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, $this->language->module_text("pdf_confidentiality").":");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", CONFIDENTIALITY_SCORE));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, $this->language->global_text("value").":");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(135, 5, implode(", ", ASSET_VALUE_LABELS));

			$pdf->Ln(3);

			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, $this->language->module_text("pdf_willingness_chance").":");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", ACTOR_CHANCE));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(40, 5, $this->language->module_text("pdf_knowledge").":");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", ACTOR_KNOWLEDGE));

			/* Threats
			 */
			$pdf->AddPage();
			$pdf->AddChapter($this->language->module_text("pdf_possible_threats"));
			$pdf->Ln(2);

			$category_id = 0;
			foreach ($templates as $template) {
				if ($template["category_id"] != $category_id) {
					$category_id = $template["category_id"];
					$pdf->Ln(1);
					$pdf->SetFillColor(232, 232, 232);
					$pdf->SetFont("helvetica", "B", 10);
					$pdf->Cell(0, 5, $categories[$category_id]["name"], 0, 0, "", true);
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Ln(5);
				}
				$pdf->Cell(7, 5, $template["number"].".");
				$pdf->Cell(0, 5, $template["threat"]);
				$pdf->Ln(5);
				$pdf->SetLeftMargin(22);
				$pdf->SetFont("helvetica", "I", 9);
				$pdf->Write(4, $template["description"]);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->SetLeftMargin(15);
				$pdf->Ln(4);
			}

			return $pdf;
		}
	}
?>
