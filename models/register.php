<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class register_model extends Banshee\splitform_model {
		const MINIMUM_USERNAME_LENGTH = 4;
		const MINIMUM_FULLNAME_LENGTH = 4;

		protected $forms = array(
			"email"   => array("email"),
			"code"    => array("code"),
			"account" => array("fullname", "username", "password", "organisation"));

		public function reset_form_progress() {
			unset($_SESSION["register_email"]);
			unset($_SESSION["register_code"]);

			parent::reset_form_progress();
		}

		public function validate_email($data) {
			$result = true;

			if (valid_email($data["email"]) == false) {
				$this->view->add_message($this->language->module_text("error_email_invalid"));
				$result = false;
			}

			$query = "select * from users where email=%s";
			if ($this->db->execute($query, $data["email"]) != false) {
				$this->view->add_message($this->language->module_text("error_email_exists"));
				$result = false;
			}

			return $result;
		}

		public function validate_code($data) {
			if ($data["code"] != $_SESSION["register_code"]) {
				$this->view->add_message($this->language->module_text("error_code_invalid"));
				return false;
			}
			
			return true;
		}

		public function validate_account($data) {
			$result = true;

			$length_okay = (strlen($data["username"]) >= self::MINIMUM_USERNAME_LENGTH);
			$format_okay = valid_input($data["username"], VALIDATE_NONCAPITALS.VALIDATE_NUMBERS."@.-_", VALIDATE_NONEMPTY);

			if (($length_okay == false) || ($format_okay == false)) {
				$this->view->add_message($this->language->module_text("error_username_short_format"), self::MINIMUM_USERNAME_LENGTH);
				$result = false;
			}

			$query = "select * from users where username=%s";
			if ($this->db->execute($query, $data["username"]) != false) {
				$this->view->add_message($this->language->module_text("error_username_exists"));
				$result = false;
			}

			if (is_secure_password($data["password"], $this->view, $this->language) == false) {
				$result = false;
			}

			if (strlen($data["fullname"]) < self::MINIMUM_FULLNAME_LENGTH) {
				$this->view->add_message($this->language->module_text("error_fullname_short"), self::MINIMUM_FULLNAME_LENGTH);
				$result = false;
			}

			if (DEFAULT_ORGANISATION_ID == 0) {
				if (trim($data["organisation"] == "")) {
					$this->view->add_message($this->language->module_text("error_organisation_specify"));
					$result = false;
				} else {
					$query = "select * from organisations where name=%s";
					if ($this->db->execute($query, $data["organisation"]) != false) {
						$this->view->add_message($this->language->module_text("error_organisation_exists"));
						$result = false;
					}
				}
			}


			return $result;
		}

		public function process_form_data($data) {
			if (DEFAULT_ORGANISATION_ID == 0) {
				$organisation = array(
					"name" => $data["organisation"]);

				if ($this->borrow("cms/organisation")->create_organisation($organisation) == false) {
					$this->db->query("rollback");
					return false;
				}

				$organisation_id = $this->db->last_insert_id;
			} else {
				$organisation_id = DEFAULT_ORGANISATION_ID;
			}

			if (ENCRYPT_DATA) {
				$crypto_key = random_string(32);
				$aes = new \Banshee\Protocol\AES256($data["password"]);
				$crypto_key = $aes->encrypt($crypto_key);
			} else {
				$crypto_key = null;
			}

			$user = array(
				"organisation_id" => $organisation_id,
				"username"        => $data["username"],
				"password"        => $data["password"],
			    "status"          => USER_STATUS_ACTIVE,
				"fullname"        => $data["fullname"],
				"email"           => $data["email"],
				"crypto_key"      => $crypto_key,
				"roles"           => array(ORGANISATION_ADMIN_ROLE_ID));

			if ($this->borrow("cms/user")->create_user($user, true) == false) {
				if (DEFAULT_ORGANISATION_ID == 0) {
					$this->borrow("cms/organisation")->delete_organisation($organisation_id);
				}
				return false;
			}

			$this->user->log_action("user %s registered", $data["username"]);

			unset($_SESSION["register_email"]);
			unset($_SESSION["register_code"]);

			$email = new \Banshee\Protocol\email("New account registered at ".$_SERVER["SERVER_NAME"], $this->settings->webmaster_email);
			$email->set_message_fields(array(
				"FULLNAME"     => $data["fullname"],
				"EMAIL"        => $data["email"],
				"USERNAME"     => $data["username"],
				"ORGANISATION" => $data["organisation"],
				"WEBSITE"      => $this->settings->head_title,
				"IP_ADDR"      => $_SERVER["REMOTE_ADDR"]));
			$email->message(file_get_contents("../extra/account_registered.txt"));
			$email->send($this->settings->webmaster_email);

			return true;
		}
	}
?>
