<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Matrix template
//
//-->
<xsl:template match="matrix">
<p><xsl:value-of select="/output/language/module/matrix_as_used_by_ravib" />:</p>
<table class="matrix">
<tr><td></td><td></td><td colspan="{count(row)-1}"><xsl:value-of select="/output/language/global/impact" /></td></tr>
<xsl:for-each select="row">
<tr>
	<xsl:if test="position()=1"><td></td></xsl:if>
	<xsl:if test="position()=2"><td rowspan="{count(../row)-1}" class="chance"><xsl:value-of select="/output/language/global/chance" /></td></xsl:if>
	<xsl:for-each select="cell">
		<td><xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if><xsl:value-of select="." /></td>
	</xsl:for-each>
</tr>
</xsl:for-each>
</table>

<xsl:value-of select="/output/language/module/approaches" disable-output-escaping="yes" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/risk_matrix" /></h1>
<xsl:apply-templates select="matrix" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
