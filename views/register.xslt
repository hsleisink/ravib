<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/splitform.xslt" />


<!--
//
//  Layout templates
//
//-->
<xsl:template name="splitform_header">
<h1><xsl:value-of select="/output/language/module/register" /></h1>
</xsl:template>

<xsl:template name="splitform_footer">
</xsl:template>

<!--
//
//  E-mail form template
//
//-->
<xsl:template match="splitform/form_email">
<p><xsl:value-of select="/output/language/module/explanation" /></p>
<label for="email"><xsl:value-of select="/output/language/module/email_address" />:</label>
<input type="input" id="email" name="email" value="{email}" class="form-control" />
</xsl:template>

<!--
//
//  Code form template
//
//-->
<xsl:template match="splitform/form_code">
<p><xsl:value-of select="/output/language/module/registeremail_with_code_sent" /></p>
<label for="code"><xsl:value-of select="/output/language/module/verification_code" />:</label>
<input type="text" name="code" value="{code}" class="form-control" />
</xsl:template>

<!--
//
//  Account form template
//
//-->
<xsl:template match="splitform/form_account">
<label for="fullname"><xsl:value-of select="/output/language/module/fullname" />:</label>
<input type="input" id="fullname" name="fullname" value="{fullname}" maxlength="50" class="form-control" />
<label for="username"><xsl:value-of select="/output/language/module/username" />:</label>
<input type="input" id="username" name="username" value="{username}" maxlength="50" class="form-control" style="text-transform:lowercase" />
<label for="password"><xsl:value-of select="/output/language/module/password" />:</label>
<input type="password" id="password" name="password" class="form-control" />
<xsl:if test="../../../ask_organisation='yes'">
<label for="organisation"><xsl:value-of select="/output/language/module/organisation" />:</label>
<input type="text" id="organisation" name="organisation" value="{organisation}" maxlength="50" class="form-control" />
</xsl:if>
</xsl:template>

<!--
//
//  Process template
//
//-->
<xsl:template match="submit">
<xsl:call-template name="splitform_header" />
<xsl:call-template name="progressbar" />
<p><xsl:value-of select="/output/language/module/account_created" /></p>
<xsl:call-template name="redirect"><xsl:with-param name="url" /></xsl:call-template>
</xsl:template>

</xsl:stylesheet>
