<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs bia">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/information_system" /></th>
<th><xsl:value-of select="/output/language/global/availability" /></th>
<th><xsl:value-of select="/output/language/global/integrity" /></th>
<th><xsl:value-of select="/output/language/global/confidentiality" /></th>
<th><xsl:value-of select="/output/language/global/value" /></th>
<th><xsl:value-of select="/output/language/global/owner" /></th>
<th><xsl:value-of select="/output/language/global/pii" /></th>
<th><xsl:value-of select="/output/language/global/location" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="item">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><span class="table-xs"><xsl:value-of select="/output/language/global/information_system" /></span><xsl:value-of select="item" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/availability" /></span><xsl:value-of select="availability" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/integrity" /></span><xsl:value-of select="integrity" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/confidentiality" /></span><xsl:value-of select="confidentiality" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/value" /></span><xsl:value-of select="value" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/owner" /></span><xsl:value-of select="owner" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/pii" /></span><xsl:value-of select="personal_data" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/location" /></span><xsl:value-of select="location" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/{/output/page}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_information_system" /></a>
<a href="/dashboard" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="item/@id">
<input type="hidden" name="id" value="{item/@id}" />
</xsl:if>

<div class="form-group">
<label for="item"><xsl:value-of select="/output/language/global/information_system" />:</label>
<input type="text" id="item" name="item" value="{item/item}" class="form-control" />
</div>
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="item/description" /></textarea>
</div>
<div class="form-group">
<label for="confidentiality"><xsl:value-of select="/output/language/global/confidentiality" />:</label>
<select id="confidentiality" name="confidentiality" class="form-control">
<option value="0"></option>
<xsl:for-each select="confidentiality/label">
	<option value="{position()}"><xsl:if test="position()=../../item/confidentiality"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="integrity"><xsl:value-of select="/output/language/global/integrity" />:</label>
<select id="integrity" name="integrity" class="form-control">
<option value="0"></option>
<xsl:for-each select="integrity/label">
	<option value="{position()}"><xsl:if test="position()=../../item/integrity"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="availability"><xsl:value-of select="/output/language/global/availability" />:</label>
<select id="availability" name="availability" class="form-control">
<option value="0"></option>
<xsl:for-each select="availability/label">
	<option value="{position()}"><xsl:if test="position()=../../item/availability"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="impact"><xsl:value-of select="/output/language/module/impact_of_incident" />:</label>
<textarea id="impact" name="impact" class="form-control"><xsl:value-of select="item/impact" /></textarea>
</div>
<div class="form-group">
<label for="owner"><xsl:value-of select="/output/language/module/assigned_to_owner" />:</label>
<div><input type="checkbox" id="owner" name="owner"><xsl:if test="item/owner='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></div>
</div>
<div class="form-group">
<label for="owner"><xsl:value-of select="/output/language/module/contains_pii" />:</label>
<div><input type="checkbox" id="personal_data" name="personal_data"><xsl:if test="item/personal_data='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></div>
</div>
<div class="form-group">
<label for="location"><xsl:value-of select="/output/language/global/location" />:</label>
<select id="location" name="location" class="form-control">
<xsl:for-each select="location/label">
	<option value="{position()-1}"><xsl:if test="(position()-1)=../../item/location"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_information_system}" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="item/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_information_system}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/bia.png" class="title_icon" />
<h1><xsl:value-of select="/output/language/module/business_impact_analysis" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<div id="help"><xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" /></div>
</xsl:template>

</xsl:stylesheet>
