<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}/{../case/@id}" method="post">
<div class="row">
<div class="col-sm-6">
<div><input type="checkbox" name="extra_info" /><xsl:value-of select="/output/language/module/extra_explanation" /></div>
<div><input type="checkbox" name="accepted_risks" /><xsl:value-of select="/output/language/module/accepted_threats" /></div>
<div><input type="checkbox" name="sort_by_risk" checked="checked" /><xsl:value-of select="/output/language/module/sort_by_risk_urgency" /></div>
</div>
<div class="col-sm-6 backup alert alert-warning">
<xsl:value-of select="/output/language/module/make_backup_1" />&#160;<a href="/data"><xsl:value-of select="/output/language/module/make_backup_2" /></a>&#160;<xsl:value-of select="/output/language/module/make_backup_3" />
</div>
</div>

<div class="btn-group left">
<input type="submit" name="submit_button" value="{/output/language/module/btn_generate_report}" class="btn btn-default" />
</div>
</form>

<div class="btn-group right">
<a href="/case/progress/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_continue" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/module/report" /></h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
