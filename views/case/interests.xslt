<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />
<div class="interests"><xsl:value-of select="." /></div>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/edit" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_edit" /></a>
<a href="/{/output/page}/{../case/@id}/handout" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_generate_handout" /></a>
</div>
<xsl:if test=".!=''">
<div class="btn-group right">
<a href="/case/risks/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_continue" /></a>
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<form action="/{/output/page}/{../case/@id}" method="post">
<textarea name="interests" class="form-control"><xsl:value-of select="." /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save}" class="btn btn-default" />
<xsl:if test=".!=''">
<a href="/{/output/page}/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/module/interests" /></h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text" /></p>
</div>
</xsl:template>

</xsl:stylesheet>
