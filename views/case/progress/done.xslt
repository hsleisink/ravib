<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../../banshee/main.xslt" />

<!--
//
//  Form template
//
//-->
<xsl:template match="form">
<form action="/{/output/page}" method="post">
<input type="hidden" name="code" value="{code}" />
<p><xsl:value-of select="/output/language/module/" />&#160;<span class="control"><xsl:value-of select="control" /></span>,<xsl:if test="name">&#160;<xsl:value-of select="/output/language/module/message_2" />&#160;<span class="name"><xsl:value-of select="name" /></span>&#160;<xsl:value-of select="/output/language/module/message_3" /></xsl:if>&#160;<xsl:value-of select="/output/language/module/message_4" />&#160;<xsl:value-of select="fullname" /><xsl:value-of select="/output/language/module/message_5" /></p>
<xsl:if test="info">
<p class="info"><xsl:value-of select="info" /></p>
</xsl:if>
<input type="submit" name="submit_button" value="Gereedmelden" class="btn btn-default" onClick="javascript:return confirm('GEREEDMELDEN: Weet u het zeker?')" />
</form>
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<p><xsl:value-of select="."/></p>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/task_done" /></h1>
<xsl:apply-templates select="form" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
