<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs risks">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/risk" /></th>
<th><xsl:value-of select="/output/language/global/approach" /></th>
<th><xsl:value-of select="/output/language/global/urgency" /></th>
<th><xsl:value-of select="/output/language/module/controls" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="risks/risk">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{/output/content/case/@id}/{@id}'">
<td><span class="table-xs"><xsl:value-of select="/output/language/global/risk" /></span><xsl:value-of select="threat" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/approach" /></span><xsl:value-of select="handle" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/urgency" /></span><span class="urgency{risk_value}"><xsl:value-of select="risk_label" /></span></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/controls" /></span><xsl:value-of select="controls" /><xsl:if test="warning='yes'"><img src="/images/warning.png" class="warning" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<xsl:if test="(count(risks/risk)>0) and (count(risks/risk[warning='yes'])=0)">
<div class="btn-group right">
<a href="/case/scenarios/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_continue" /></a>
</div>
</xsl:if>

<div id="help"><xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" /></div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<h2><xsl:value-of select="threat/threat" /></h2>
<div class="row">
<div class="col-sm-4"><label><xsl:value-of select="/output/language/global/chance" />:</label> <xsl:value-of select="threat/chance" /></div>
<div class="col-sm-4"><label><xsl:value-of select="/output/language/global/impact" />:</label> <xsl:value-of select="threat/impact" /></div>
<div class="col-sm-4"><label><xsl:value-of select="/output/language/module/chosen_approach" />:</label> <xsl:value-of select="threat/handle" /></div>
</div>
<label><xsl:value-of select="/output/language/global/current_situation" />:</label>
<p class="control"><xsl:value-of select="threat/current" /></p>
<label><xsl:value-of select="/output/language/global/desired_situation" />:</label>
<p class="control"><xsl:value-of select="threat/action" /></p>

<form action="/{/output/page}/{../case/@id}" method="post">
<input type="hidden" name="case_risk_id" value="{threat/@id}" />
<h2><xsl:value-of select="controls/@standard" /><span class="show_highlighter" onClick="javascript:$('div.highlighter').toggle()">+</span></h2>
<div class="row highlighter">
<div class="col-md-3 col-sm-4 col-xs-12"><xsl:value-of select="/output/language/module/highlight_controls_for" />:</div>
<div class="col-md-9 col-sm-8 col-xs-12"><select class="form-control threats" onChange="javascript:highlight(this)">
<xsl:for-each select="threats/threat">
<option value="{@id}"><xsl:value-of select="." /></option>
</xsl:for-each>
</select></div>
</div>
<table class="table table-condensed table-striped controls">
<thead>
<tr>
<th>#</th>
<th><xsl:value-of select="/output/language/module/control" /></th>
<th><xsl:value-of select="/output/language/module/reduces" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="controls/control">
<tr class="control_{@id}"><xsl:if test="effective='no'"><xsl:attribute name="class">ineffective</xsl:attribute></xsl:if>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="reduces" /></td>
<td><input type="checkbox" name="selected[]" value="{@id}"><xsl:if test="selected='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save}" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/module/controls" /></h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
