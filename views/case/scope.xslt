<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}/{../case/@id}" method="post">
<table class="table table-condensed table-striped table-xs scope">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/information_system" /></th>
<th><xsl:value-of select="/output/language/global/value" /></th>
<th><xsl:value-of select="/output/language/module/owner" /></th>
<th><xsl:value-of select="/output/language/global/pii" /></th>
<th><xsl:value-of select="/output/language/global/location" /></th>
<th class="visible last"></th>
<th class="hidden last"><xsl:value-of select="/output/language/module/scope" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="item[scope='yes']">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/information_system" /></span><xsl:value-of select="item" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/value" /></span><xsl:value-of select="value" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/owner" /></span><xsl:value-of select="owner" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/pii" /></span><xsl:value-of select="personal_data" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/location" /></span><xsl:value-of select="location" /></td>
<td class="visible"></td>
<td class="hidden"><input type="checkbox" name="scope[]" value="{@id}" checked="checked" /></td>
</tr>
</xsl:for-each>
<xsl:for-each select="item[scope='no']">
<tr class="hidden">
<td><xsl:value-of select="item" /></td>
<td><xsl:value-of select="value" /></td>
<td><xsl:value-of select="owner" /></td>
<td><xsl:value-of select="personal_data" /></td>
<td><xsl:value-of select="location" /></td>
<td class="visible"></td>
<td><input type="checkbox" name="scope[]" value="{@id}" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left visible">
<input type="button" value="{/output/language/module/btn_edit}" class="btn btn-default" onClick="javascript:enable_form()" />
</div>
<div class="btn-group left hidden">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save}" class="btn btn-default" />
<input type="button" value="{/output/language/global/btn_cancel}" class="btn btn-default" onClick="javascript:disable_form()" />
</div>
</form>

<xsl:if test="count(item[scope='yes'])>0">
<div class="btn-group right visible">
<a href="/case/interests/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_continue" /></a>
</div>
</xsl:if>

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text" /></p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/module/scope" /></h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
