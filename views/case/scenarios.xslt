<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/scenario" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="scenarios/scenario">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{../../../case/@id}/{@id}'">
<td><xsl:value-of select="title" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_scenario" /></a>
</div>
<div class="btn-group right">
<a href="/case/report/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_continue" /></a>
</div>

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text" /></p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />

<!-- Actors -->
<h2><xsl:value-of select="/output/language/module/actors" /><span class="show_table" onClick="javascript:$('table.actors').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs actors">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/module/actor" /></th>
<th><xsl:value-of select="/output/language/module/willingness_chance" /></th>
<th><xsl:value-of select="/output/language/module/knowledge" /></th>
<th><xsl:value-of select="/output/language/module/resources" /></th>
<th><xsl:value-of select="/output/language/global/threat" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/actor" /></span><xsl:value-of select="name" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/willingness_chance" /></span><xsl:value-of select="chance" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/knowledge" /></span><xsl:value-of select="knowledge" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/resources" /></span><xsl:value-of select="resources" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/threat" /></span><xsl:value-of select="threat" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<!-- Risks -->
<h2><xsl:value-of select="/output/language/global/risks" /><span class="show_table" onClick="javascript:$('table.threats').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs threats">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/risk" /></th>
<th><xsl:value-of select="/output/language/global/chance" /></th>
<th><xsl:value-of select="/output/language/global/impact" /></th>
<th><xsl:value-of select="/output/language/global/urgency" /></th>
<th><xsl:value-of select="/output/language/global/approach" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="risks/risk">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/risk" /></span><xsl:value-of select="threat" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/chance" /></span><xsl:value-of select="chance" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/impact" /></span><xsl:value-of select="impact" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/urgency" /></span><span class="urgency{risk_value}"><xsl:value-of select="risk_label" /></span></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/approach" /></span><xsl:value-of select="handle" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<!-- BIA -->
<h2><xsl:value-of select="/output/language/module/bia" /><span class="show_table" onClick="javascript:$('table.bia').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs bia">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/module/system" /></th>
<th><xsl:value-of select="/output/language/module/impact_incident" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="bia/item">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/system" /></span><xsl:value-of select="item" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/impact_incident" /></span><xsl:value-of disable-output-escaping="yes" select="impact" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<form action="/{/output/page}/{../case/@id}" method="post">
<xsl:if test="scenario/@id">
<input type="hidden" name="id" value="{scenario/@id}" />
</xsl:if>

<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{scenario/title}" class="form-control" />
</div>
<div class="form-group">
<label for="scenario"><xsl:value-of select="/output/language/module/scenario" />:</label>
<textarea id="scenario" name="scenario" class="form-control"><xsl:value-of select="scenario/scenario" /></textarea>
</div>
<div class="form-group">
<label for="consequences"><xsl:value-of select="/output/language/module/possible_consequences" />:</label>
<textarea id="consequences" name="consequences" class="form-control"><xsl:value-of select="scenario/consequences" /></textarea>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_scenario}" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="scenario/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_scenario}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/module/scenarios" /></h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
