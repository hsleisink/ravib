<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs risks">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/global/threat" /></th>
<th><xsl:value-of select="/output/language/global/chance" /></th>
<th><xsl:value-of select="/output/language/global/impact" /></th>
<th><xsl:value-of select="/output/language/global/approach" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="risks/risk">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{/output/content/case/@id}/{@id}'">
<td><span class="table-xs"><xsl:value-of select="/output/language/global/threat" /></span><xsl:value-of select="threat" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/chance" /></span><xsl:value-of select="chance" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/impact" /></span><xsl:value-of select="impact" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/approach" /></span><xsl:value-of select="handle" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/{/output/content/case/@id}/template" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_risk_from_template" /></a>
<a href="/{/output/page}/{/output/content/case/@id}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_risk" /></a>
</div>

<xsl:if test="count(risks/risk)>0">
<div class="btn-group right">
<a href="/case/controls/{../case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_continue" /></a>
</div>
</xsl:if>

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text_overview" /></p>
</div>
</xsl:template>

<!--
//
//  Template template
//
//-->
<xsl:template match="templates">
<xsl:call-template name="show_messages" />
<p><xsl:value-of select="/output/language/module/choose_from_template" /></p>

<table class="table table-condensed table-hover table-xs templates">
<thead>
<tr>
<th class="number">#</th>
<th class="threat"><xsl:value-of select="/output/language/global/threat" /></th>
<th class="cia"><xsl:value-of select="/output/language/module/a" /></th>
<th class="cia"><xsl:value-of select="/output/language/module/i" /></th>
<th class="cia"><xsl:value-of select="/output/language/module/c" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="template">
<xsl:if test="category">
<tr class="category">
<td colspan="2"><xsl:value-of select="category" /></td>
<td><xsl:value-of select="/output/language/module/a" /></td>
<td><xsl:value-of select="/output/language/module/i" /></td>
<td><xsl:value-of select="/output/language/module/c" /></td>
</tr>
</xsl:if>
<tr onClick="javascript:document.location='/{/output/page}/{../../case/@id}/new/{@id}'">
<td><xsl:value-of select="number" /></td>
<td><div class="threat"><xsl:value-of select="threat" /></div>
<div class="description"><xsl:value-of select="description" /></div></td>
<td><xsl:value-of select="availability" /></td>
<td><xsl:value-of select="integrity" /></td>
<td><xsl:value-of select="confidentiality" /></td>
</tr>
</xsl:for-each>

</tbody>
</table>

<div class="btn-group">
<a href="/{/output/page}/{../case/@id}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_dont_use_template" /></a>
<a href="/{/output/page}/{/output/content/case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text_template" /></p>I
</div>
</xsl:template>

<!--
//
//  Controls template
//
//-->
<xsl:template match="controls">
<table class="table table-condensed">
<thead>
<tr><th>#</th><th><xsl:value-of select="/output/language/global/control_from" />&#160;<xsl:value-of select="standard" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="item">
<tr>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}/{/output/content/case/@id}/{risk/template_id}" method="post">
<xsl:if test="risk/@id">
<input type="hidden" name="id" value="{risk/@id}" />
</xsl:if>
<xsl:if test="risk/template_id">
<input type="hidden" name="template_id" value="{risk/template_id}" />
</xsl:if>

<xsl:if test="risk/template">
<h2><xsl:value-of select="risk/template" /></h2>
<div class="description"><xsl:value-of select="risk/description" /></div>
<div class="row form-group">
<div class="col-sm-4"><xsl:value-of select="/output/language/global/confidentiality" />: <xsl:value-of select="risk/confidentiality" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/integrity" />: <xsl:value-of select="risk/integrity" /></div>
<div class="col-sm-4"><xsl:value-of select="/output/language/global/availability" />: <xsl:value-of select="risk/availability" /></div>
</div>
</xsl:if>
<div class="form-group">
<label for="threat"><xsl:value-of select="/output/language/global/threat" />:</label>
<input type="text" id="theat" name="threat" value="{risk/threat}" class="form-control" />
</div>
<div class="form-group">
<label for="actor"><xsl:value-of select="/output/language/module/most_threatening_actor" />:</label>
<select id="actor" name="actor_id" class="form-control">
<xsl:for-each select="actors/actor">
<option value="{@id}"><xsl:if test="@id=../../risk/actor_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>

<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="causes"><xsl:value-of select="/output/language/module/causes" />:</label>
<textarea id="causes" name="causes" class="form-control"><xsl:value-of select="risk/causes" /></textarea>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="effects"><xsl:value-of select="/output/language/module/effects" />:</label>
<textarea id="effects" name="effects" class="form-control"><xsl:value-of select="risk/effects" /></textarea>
</div>
</div>
</div>

<div class="row">
<div class="col-sm-4">
<div class="form-group">
<label for="chance"><xsl:value-of select="/output/language/global/chance" />:</label>
<select id="chance" name="chance" class="form-control">
<xsl:for-each select="chance/option">
<option value="{@value}"><xsl:if test="@value=../../risk/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div>
<div class="col-sm-4">
<div class="form-group">
<label for="impact"><xsl:value-of select="/output/language/global/impact" />:</label>
<select id="impact" name="impact" class="form-control">
<xsl:for-each select="impact/option">
<option value="{@value}"><xsl:if test="@value=../../risk/impact"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div>
<div class="col-sm-4">
<div class="form-group">
<label for="handle"><xsl:value-of select="/output/language/global/approach" />:</label>
<select id="handle" name="handle" class="form-control">
<xsl:for-each select="handle/option">
<option value="{@value}"><xsl:if test="@value=../../risk/handle"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div>
</div>
<label for="argumentation"><xsl:value-of select="/output/language/module/argumentation" />:</label>
<textarea id="argumentation" name="argumentation" class="form-control"><xsl:value-of select="risk/argumentation" /></textarea>

<h2><xsl:value-of select="/output/language/module/affected_systems" /><input type="button" value="{/output/language/module/btn_check_all}" class="btn btn-default btn-xs" onClick="javascript:$('table.scope tbody input').prop('checked', true)" /></h2>
<table class="table table-condensed table-striped table-xs scope">
<thead class="table-xs">
<tr>
<th></th>
<th><xsl:value-of select="/output/language/global/information_system" /></th>
<th><xsl:value-of select="/output/language/global/availability" /></th>
<th><xsl:value-of select="/output/language/global/integrity" /></th>
<th><xsl:value-of select="/output/language/global/confidentiality" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="scope/system">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/afffected" /></span><input type="checkbox" name="risk_scope[]" value="{@id}"><xsl:if test="selected='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/information_system" /></span><xsl:value-of select="item" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/availability" /></span><xsl:value-of select="availability" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/integrity" /></span><xsl:value-of select="integrity" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/confidentiality" /></span><xsl:value-of select="confidentiality" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2><xsl:value-of select="/output/language/module/measures" /></h2>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="curent"><xsl:value-of select="/output/language/global/current_situation" />:</label>
<textarea id="current" name="current" class="form-control"><xsl:value-of select="risk/current" /></textarea>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="action"><xsl:value-of select="/output/language/global/desired_situation" />:</label>
<textarea id="action" name="action" class="form-control"><xsl:value-of select="risk/action" /></textarea>
</div>
</div>
</div>
<xsl:apply-templates select="risk/controls" />

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_risk}" class="btn btn-default" />
<a href="/{/output/page}/{/output/content/case/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="risk/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_risk}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/global/risks" /></h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="templates" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
