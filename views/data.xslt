<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<div class="row">
<div class="col-sm-6">

<div class="panel panel-default data">
<div class="panel-heading"><xsl:value-of select="/output/language/module/export_data" /></div>
<div class="panel-body">
<form action="/{/output/page}" method="post">
<input type="submit" name="submit_button" value="{/output/language/module/btn_export}" class="btn btn-default" />
</form>
</div>
</div>

</div>
<div class="col-sm-6">

<div class="panel panel-default data">
<div class="panel-heading"><xsl:value-of select="/output/language/module/import_data" /></div>
<div class="panel-body">
<form action="/{/output/page}" method="post" enctype="multipart/form-data">
<div class="input-group">
<span class="input-group-btn"><label class="btn btn-default">
<input type="file" name="file" style="display:none" class="form-control" onChange="$('#upload-file-info').val(this.files[0].name)" /><xsl:value-of select="/output/language/module/btn_browse" /></label></span>
<input type="text" id="upload-file-info" readonly="readonly" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="{/output/language/module/btn_import}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/module/are_you_sure}')" /></span>
</div>
</form>
</div>
</div>

</div>
</div>

<div class="btn-group">
<a href="/dashboard" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>

<div id="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/data.png" class="title_icon" />
<h1><xsl:value-of select="/output/language/module/data_management" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
