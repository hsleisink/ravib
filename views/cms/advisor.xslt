<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed advisors">
<thead>
<tr><th>Advisor</th><th>Organisation</th><th>Status</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="advisor">
<tr>
<td><xsl:value-of select="fullname" /></td>
<td><xsl:value-of select="organisation" /></td>
<td><xsl:if test="@ready='no'"><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Accept request" class="btn btn-primary btn-xs" onClick="javascript:return confirm('ACCEPT: Are you sure?')" /></form></xsl:if><xsl:if test="@ready='yes'">Granted</xsl:if></td>
<td><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Delete" class="btn btn-danger btn-xs" onClick="javascript:return confirm('DELETE: Are you sure?')" /></form></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/cms" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>In RAVIB it is possible for someone from another organization to act as an information security advisor. They can request access to your risk analyses via RAVIB. With this page you approve or reject requests and you withdraw permissions.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/advisor.png" class="title_icon" />
<h1>Advisors</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
