<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:if test="@archive='yes'">
<h3><xsl:value-of select="/output/language/module/archive" /></h3>
</xsl:if>
<form action="/{/output/page}" method="post">
<table class="table table-condensed table-striped">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/subject" /></th>
<th><xsl:value-of select="/output/language/module/standard" /></th>
<th><xsl:value-of select="/output/language/module/date" /></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="case">
<tr>
<td><xsl:if test="organisation!=''"><xsl:value-of select="organisation" /> :: </xsl:if><xsl:value-of select="name" /></td>
<td><xsl:value-of select="standard" /></td>
<td><xsl:value-of select="date" /></td>
<td><span class="btn-group"><a href="/{start}/{@id}" id="start" class="btn btn-xs btn-primary"><xsl:value-of select="/output/language/module/btn_start" /></a> <a href="/{/output/page}/{@id}" class="btn btn-xs btn-default"><xsl:value-of select="/output/language/module/btn_edit" /></a></span></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<xsl:if test="@archive='no'">
<a href="/{/output/page}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_case" /></a>
<xsl:if test="@archived>0">
<a href="/{/output/page}/archive" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_show_archive" /></a>
</xsl:if>
<a href="/dashboard" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</xsl:if>
<xsl:if test="@archive='yes'">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_leave_archive" /></a>
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="case/@id">
<input type="hidden" name="id" value="{case/@id}" />
</xsl:if>

<div class="row">
<div class="col-md-6">
<div class="form-group">
<label for="name"><xsl:value-of select="/output/language/module/organisational_unit" />:</label>
<input type="text" id="organisation" name="organisation" value="{case/organisation}" class="form-control" />
</div>
<div class="form-group">
<label for="name"><xsl:value-of select="/output/language/module/case_name" />:</label>
<input type="text" id="name" name="name" value="{case/name}" class="form-control" />
</div>
<div class="form-group">
<label for="standard_id"><xsl:value-of select="/output/language/module/standard" />:</label>
<select id="standard_id" name="standard_id" class="form-control">
<xsl:for-each select="standards/standard">
<option value="{@id}"><xsl:if test="@id=../../case/standard_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="date"><xsl:value-of select="/output/language/module/date" />:</label>
<input type="text" id="date" name="date" value="{case/date}" class="form-control datepicker" />
</div>
<div class="form-group">
<label for="scope"><xsl:value-of select="/output/language/module/scope_limited_to" /></label>
<textarea id="scope" name="scope" class="form-control"><xsl:value-of select="case/scope" /></textarea>
</div>
<div class="form-group">
<label for="logo"><xsl:value-of select="/output/language/module/logo_url" />:</label>
<input type="text" id="logo" name="logo" value="{case/logo}" maxlength="250" class="form-control" />
</div>
<xsl:if test="case/@id">
<div class="form-group">
<label for="archived"><xsl:value-of select="/output/language/module/archived" />:</label>
<input type="checkbox" id="archived" name="archived"><xsl:if test="case/archived='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
</div>
</xsl:if>
</div>

<div class="col-md-6">
<h3><xsl:value-of select="/output/language/module/impact_interpretation" /></h3>
<p><xsl:value-of select="/output/language/module/impact_explanation" /></p>
<xsl:for-each select="impact/value">
<div class="form-group">
<label for="impact_{position()}"><xsl:value-of select="@label" />:</label>
<input type="text" id="impact_{position()}" name="impact[]" value="{.}" class="form-control" />
</div>
</xsl:for-each>
</div>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_case}" class="btn btn-default" />
<xsl:if test="case/archived='no'">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
<xsl:if test="case/archived='yes'">
<a href="/{/output/page}/archive" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
<xsl:if test="case/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_case}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/case.png" class="title_icon" />
<h1><xsl:value-of select="/output/language/module/cases" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
