<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs actors">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/module/actor" /></th>
<th><xsl:value-of select="/output/language/module/willingness_chance" /></th>
<th><xsl:value-of select="/output/language/module/knowledge" /></th>
<th><xsl:value-of select="/output/language/module/resources" /></th>
<th><xsl:value-of select="/output/language/global/threat" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><span class="table-xs"><xsl:value-of select="/output/language/module/actor" /></span><xsl:value-of select="name" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/willingness_chance" /></span><xsl:value-of select="chance" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/knowledge" /></span><xsl:value-of select="knowledge" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/resources" /></span><xsl:value-of select="resources" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/global/threat" /></span><xsl:value-of select="threat" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group">
<a href="/{/output/page}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_actor" /></a>
<a href="/dashboard" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="actor/@id">
<input type="hidden" name="id" value="{actor/@id}" />
</xsl:if>

<div class="form-group">
<label for="name"><xsl:value-of select="/output/language/module/actor_name" />:</label>
<input type="text" id="name" name="name" value="{actor/name}" class="form-control" />
</div>
<div class="form-group">
<label for="chance"><xsl:value-of select="/output/language/module/willingness_attack_chance_incident" />:</label>
<select name="chance" class="form-control">
<xsl:for-each select="chance/item"><option value="{position()}"><xsl:if test="position()=../../actor/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="knowledge"><xsl:value-of select="/output/language/module/knowledge" />:</label>
<select name="knowledge" class="form-control">
<xsl:for-each select="knowledge/item"><option value="{position()}"><xsl:if test="position()=../../actor/knowledge"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="resources"><xsl:value-of select="/output/language/module/available_resources" />:</label>
<select name="resources" class="form-control">
<xsl:for-each select="resources/item"><option value="{position()}"><xsl:if test="position()=../../actor/resources"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="reason"><xsl:value-of select="/output/language/module/target_cause" />:</label>
<input type="text" id="reason" name="reason" value="{actor/reason}" maxlength="500" class="form-control" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_actor}" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="actor/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_actor}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/actors.png" class="title_icon" />
<xsl:apply-templates select="breadcrumbs" />
<h1><xsl:value-of select="/output/language/module/actors" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<div id="help"><xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" /></div>
</xsl:template>

</xsl:stylesheet>
