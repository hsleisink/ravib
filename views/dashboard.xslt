<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Dashboard template
//
//-->
<xsl:template match="dashboard">
<div class="row">
<div class="col-sm-10">

<div class="row">
<div class="col-sm-6">
<h2><xsl:value-of select="/output/language/module/not_accepted_risks" /></h2>
<table class="table table-condensed table-striped risks">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/level" /></th>
<th><xsl:value-of select="/output/language/module/count" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="risks/risk">
<tr>
<td><xsl:value-of select="." /></td>
<td><xsl:value-of select="@value" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>
<div class="col-sm-6">
<h2><xsl:value-of select="/output/language/module/selected_controls" /></h2>
<table class="table table-condensed table-striped controls">
<thead>
<tr>
<th><xsl:value-of select="/output/language/global/urgency" /></th>
<th><xsl:value-of select="/output/language/module/count" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="controls/control">
<tr>
<td><xsl:value-of select="." /></td>
<td><xsl:value-of select="@value" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>
</div>

<h2><xsl:value-of select="/output/language/module/risks_to_information_systems" /></h2>
<table class="table table-condensed table-striped bia">
<thead>
<tr><th><xsl:value-of select="/output/language/global/information_system" /></th>
<xsl:for-each select="labels/label">
<th><xsl:value-of select="." /></th>
</xsl:for-each>
</tr>
</thead>
<tbody>
<xsl:for-each select="systems/system">
<tr>
<td><xsl:value-of select="item" /></td>
<td><xsl:value-of select="risk3" /></td>
<td><xsl:value-of select="risk2" /></td>
<td><xsl:value-of select="risk1" /></td>
<td><xsl:value-of select="risk0" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2><xsl:value-of select="/output/language/module/control_implementation_progress" /></h2>
<div class="progress">
	<xsl:if test="done>0">
	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{done}" aria-valuemin="0" aria-valuemax="100" style="width:{done}%" title="Voltooid"><xsl:value-of select="done" />%</div>
	</xsl:if>
	<xsl:if test="pending>0">
	<div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="{pending}" aria-valuemin="0" aria-valuemax="100" style="width:{pending}%" title="Ingepland"><xsl:value-of select="pending" />%</div>
	</xsl:if>
	<xsl:if test="overdue>0">
	<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{overdue}" aria-valuemin="0" aria-valuemax="100" style="width:{overdue}%" title="Voorbij deadline"><xsl:value-of select="overdue" />%</div>
	</xsl:if>
	<xsl:if test="idle>0">
	<div class="progress-bar progress-bar-idle" role="progressbar" aria-valuenow="{idle}" aria-valuemin="0" aria-valuemax="100" style="width:{idle}%" title="Niet ingepland"><xsl:value-of select="idle" />%</div>
	</xsl:if>
</div>

</div>
<div class="col-sm-2">
<xsl:if test="menu/item">
<div class="panel panel-default">
<div class="panel-heading"><xsl:value-of select="/output/language/module/menu" /></div>
<div class="panel-body">
<div class="row menu">
<xsl:for-each select="menu/item">
<div class="col-xs-6 col-sm-12"><a href="/{@link}"><img src="/images/icons/{@link}.png" /></a><xsl:value-of select="." /></div>
</xsl:for-each>
</div>
</div>
</div>
</xsl:if>

</div>
</div>

<div id="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/dashboard" /></h1>
<xsl:apply-templates select="dashboard" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
