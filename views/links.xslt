<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="includes/standards.xslt" />

<!--
//
//  Links template
//
//-->
<xsl:template match="links">
<xsl:apply-templates select="standards" />

<ul class="tabs">
<li class="selected" id="threat" onClick="show_threats()"><xsl:value-of select="/output/language/module/threats" /></li>
<li id="control" onClick="show_controls()"><xsl:value-of select="/output/language/module/controls" /></li>
</ul>

<div class="threats">
<p><xsl:value-of select="/output/language/module/threats_header_1" />&#160;<xsl:value-of select="standards/standard[@selected='yes']" />&#160;<xsl:value-of select="/output/language/module/threats_header_2" /></p>
<xsl:for-each select="threats/threat">
<xsl:if test="category">
<div class="item">
<h3><xsl:value-of select="category" /></h3>
</div>
</xsl:if>
<div class="item">
<div class="head" onClick="javascript:$('.controls_{@id}').slideToggle('normal')">
	<xsl:value-of select="number" />. <xsl:value-of select="threat" />
</div>
<div class="links controls_{@id}">
<p><xsl:value-of select="description" /></p>
<div class="row">
<div class="col-sm-4 col-xs-12"><xsl:value-of select="/output/language/global/availability" />: <xsl:value-of select="availability" /></div>
<div class="col-sm-4 col-xs-12"><xsl:value-of select="/output/language/global/integrity" />: <xsl:value-of select="integrity" /></div>
<div class="col-sm-4 col-xs-12"><xsl:value-of select="/output/language/global/confidentiality" />: <xsl:value-of select="confidentiality" /></div>
</div>
<ul>
<xsl:for-each select="control">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</div>
</div>
</xsl:for-each>
</div>

<div class="controls">
<p><xsl:value-of select="/output/language/module/contols_header_1" />&#160;<xsl:value-of select="standards/standard[@selected='yes']" />&#160;<xsl:value-of select="/output/language/module/controls_header_2" /></p>
<xsl:for-each select="controls/control">
<xsl:if test="category">
<div class="item">
<h3><xsl:value-of select="category" /></h3>
</div>
</xsl:if>
<div class="item">
<div class="head" onClick="javascript:$('.threats_{@id}').slideToggle('normal')">
	<xsl:value-of select="number" />&#160;<xsl:value-of select="control" />
</div>
<div class="links threats_{@id}">
<p><xsl:value-of select="/output/language/global/reduces_1" />&#160;<xsl:value-of select="reduces" />&#160;<xsl:value-of select="/output/language/global/reduces_2" /></p>
<ul>
<xsl:for-each select="threat">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</div>
</div>
</xsl:for-each>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/links" /></h1>
<xsl:apply-templates select="links" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
